<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Data directory](#markdown-header-data-directory)
- [Pseudoautosomal regions (PAR)](#markdown-header-pseudoautosomal-regions-par)
- [Contents](#markdown-header-contents)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Data directory

Files in this directory represent reference data that changes rarely,
and is not too voluminous (so no complete genomes or GTF files; they 
are too big and don't need versioning by us anyways)

# Pseudoautosomal regions (PAR)

Unlike the human assembly, the PARs on the mouse Y chromosome are not
masked; they are simply (and deliberately) identical to the homologous region on X (i.e. they
use one assembly for this region, just like the autosomes.
To avoid multimappers here, mouse Y-PAR must be me masked. The coordinates can be found
at https://www.ncbi.nlm.nih.gov/grc/mouse).

# Contents

 * README.md: this file

 * celseq2_bc384-v4.csv: the well barcodes. For details, see inside that file

 * *.json: configuration files for the pipeline. In addition tothe
   general informatino, there should be one per species. We use one
   genome for mouse, including all the extra constructs it may contain.

 * directory **sequences**: files in this directory represent sequences
   such as those of ERCCs and artificial constructs used in mouse
   models. See there for details.

 * application.conf.hpc and application.conf.local: versions of the cromwell configuration files to cope with bad HPC nodes. Copy them to `/hpc/local/CentOS7/gen/software/wdl-29.0.12wonkynodes` (which currently corresponds to the lua module `wdl/666`
