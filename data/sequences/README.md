<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Data directory](#markdown-header-data-directory)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Data directory

Files in this directory represent sequences such as those of ERCCs and
artificial constructs used in mouse models. **do not put genome data here, it is too voluminous** (and does not need to be versioned either).

 * ERCC92.fa - sequences of the external controls

 * ERCC92.gtf - 'fake' GTF file for them (needed by STAR/featureCounts; create using e.g. `Sharq/miscUtilities/fasta2gtf.pl`


 * EGFP_IRES_Cre__*.fa - GFP construct used in some mouse models. There are two versions: _singleexon (one gene consisting of one transcript consisting of one exon, all of the same full length), and multiexon, where the construct is represented as one gene containig one transcript consisting of 5 exons: 
    1. EGFP
    2. IRES
    3. Cre
    4. ERT2
    5. polyAsignal

 * NeoR__extra.{fa,gtf,gb}: NeoR construct
 * tdTom__extra.{fa,gtf,gb}: tdTomato construct


