<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [How to create a genome](#markdown-header-how-to-create-a-genome)
    - [masking the pseudoautosomal region:](#markdown-header-masking-the-pseudoautosomal-region)
    - [Adding the artificial constructs](#markdown-header-adding-the-artificial-constructs)
    - [Indexing](#markdown-header-indexing)
    - [creating the GTF files](#markdown-header-creating-the-gtf-files)
        - [protein_coding only:](#markdown-header-protein_coding-only)
    - [protein clusters:](#markdown-header-protein-clusters)
        - [antisense + lincRNA](#markdown-header-antisense-lincrna)
        - ['the rest'](#markdown-header-the-rest)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# How to create a genome

This is the code I used to create a new (mouse) genome that includes the extra artificial constructs.
Treat carefully, it's just example code that at some point worked in my (PL) hands. 

mousehome=/hpc/local/CentOS7/gen/data/genomes/mouse/gencode
cd $mousehome

## masking the pseudoautosomal region:

The Y-chromosome pseudoautosomal region (Y-PAR) is by definition
identical to that on the X-chromosome (at least with current
assemblies). However, they are given as an expclitit real sequence on
the Y-chromosome (for mouse, but maybe not for human). Reads mapping there
would therefore be multimapping, and must therefore be discarded. To avoid this, mask
the Y-PAR as follows:


```bash
nice bedtools  maskfasta -fi GRCm38.p5ERCC.genome.fa -bed Y-pseudo-autosomal-region/Ypar.bed -fo GRCm38.p5ERCC-PARmasked.genome.fa 
```

The coordinates (here: `Ypar.bed`) can be found in https://www.ncbi.nlm.nih.gov/grc/mouse and/or https://www.ncbi.nlm.nih.gov/grc/human (search for PAR)

(note: I used a genome that already contained the ERCCs)

Define where to get the extra data from (things like artificial constructs):

```bash
refdata=$HOME/git/Sharq/data/sequences
```

From parYmasked, get just the chromosomes named 'chr' (same as primary
but without the unassembled scaffolds). 

```bash

genome=GRCm38chronly-parYmasked.genome.fa  # name to be used for our 'naked' genome

cat GRCm38.p5-parYmasked-ERCC-EGFP.genome.fa |\
   ~/git/tools/sequence/seqconvert -from fa -to tab |\
    grep ^chr | ~/git/tools/sequence/seqconvert -from tab -to fa \
   > $genome   ## note: this will get rid of the ERCC's as well, but they'll be added back later
```

## Adding the artificial constructs

We can now add the sequences any extra constructs we want. Each
artificial constructs has their own 'chromosome': ERCC's all start with
`ERCC-`, things like GFP constructs all end in `__extra`.  (A 
bonus is that with STAR, you don't have to assign reads to
features, because the mapping to these fictitious chromosomes is already
visible in the BAM file).

```bash
extension=IRES_Cre_EGFP_tdTomato_NeoR_ERCC            # shorthand for all our extensions

extendedgenome=GRCm38chronly_parYmasked_$extension.fa # name to be used for our extended genome

(cat $refdata/EGFP_IRES_Cre__extra.fa
 cat $refdata/tdTomato__extra.fa 
 cat $refdata/NeoR__extra.fa 
 cat $refdata/ERCC92.fa 
 cat $genome) > $extendedgenome   # no gzipping!
   
gtf=$mousehome/gencode.vM14.annotation.gtf   # The source GTF annotation file to use later on
```

## Indexing

The big fasta file should now be ready for indexing. Define a name for
the indexes; this is a location where STAR indexing will put the
indexes, and where STAR mapping will look for indexes:

```bash
export genomeDir=$mousehome/mouse_$extension  # this defines the place where STAR will look for the genome
mkdir $genomeDir
```

We can specify a database (in the form of a GTF file) of
splice-junctions for STAR to use. Since our extra constructs don't have
them, we can simply use the big GTF file as the splice-junction database.

Now do the actual indexing:

```bash
module load STAR

resources="-l h_rt=06:00:00 -l h_vmem=40G -l tmpspace=200G -p threaded=4 "
qrun.sh   $resources -N 'idx' "STAR --runThreadN 4 --runMode genomeGenerate \
   --outTmpDir \$TMPDIR/tmp \
   --genomeDir $genomeDir \
   --sjdbOverhang 74 \
   --genomeFastaFiles $mousehome/$extendedgenome \
   --sjdbGTFfile $mousehome/$gtf   > starindexing.out 2> starindexing.log "
```

## creating the GTF files

There are several GTF files to be created, each containing different
feature types (so that we can do hierarchical asssignments)

### protein_coding only:

The extra constructs are added to the feature type 'protein-coding', and
we therefore need a GTF file (that essentially says: my artificial
features runs from 1 to the length of my fictitious chromosome). 
Start with a the header, then add the real protein coding features from the source gtf, 
then add our artificial things. In the last step, everything has to be sorted by chromsome and coordinates:

```bash
version=vm14-$extension
extendedgtf=gencode$version-protein_coding.gtf
( 
  head -10 $gtf  | sed "/^##.*description/s/##.*/##description: gencode $version, protein_coding /;"
  grep 'gene_type "protein_coding"' $gtf 
  cat $refdata/EGFP_IRES_Cre__singleexon.gtf
  cat $refdata/tdTom__singleexon.gtf
  cat $refdata/NeoR__singleexon.gtf
  cat $refdata/ERCC92.gtf 
) | sort -k1,1 -k4,4n -k5,5n > $extendedgtf
```


## protein clusters:

I downloaded GOassoc data as described in the usage message of
gtfClustering/cluster-GTFprotein_coding.R, put it in
GOassoc/GRCm38.p5-8aug2017.txt, then interactively created the
protein_coding clusters file using `gtfClustering/cluster-GTFprotein_coding.R`. 
This was followed by a rename.

### antisense + lincRNA
```bash
( 
  head -10 $gtf  | grep '^#'  | sed '/^#.*description/s/##.*/##description: gencode $version, antisense+lincRNA /;' 
  egrep 'gene_type "(antisense|lincRNA)"' $gtf
) | sort -k1,1 -k4,4n -k5,5n > gencode$version-antisense+lincRNA.gtf
```

### 'the rest'

```bash
( 
  head -10 $gtf  | grep '^#'  | sed '/^#.*description/s/##.*/##description: gencode $version, all but protein_coding, antisense or lincRNA /;' 

  grep -v '^##' $gtf  | egrep -v 'gene_type "(protein_coding|antisense|lincRNA)"' 

)  | sort -k1,1 -k4,4n -k5,5n > gencode$version-rest.gtf
```
