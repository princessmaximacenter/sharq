#!/usr/bin/env python
import os
import math
import functools
import operator
import numpy
import argparse
import sys
import itertools
import json
import gzip

from TUtils import HPCJobs, cleanFile, PushDir, ePrint, getVersion, writeDelimitedJSON

## script that does a number of trimming operations on fastq input file, in turn:
## (1) get rid long stretches of A,C,G,T (and anything beyond)
## (2) get rid of trailing low quality bases
## (3) get rid of trailing stretches of low complexity
## Reads that, after this ordeal, are too short are discarded.
## The script is typically called from within the multiCompTrimming.py driver script

qualityDict = {"!": 0, "\"": 1, "#": 2, "$": 3, "%": 4, "&": 5,
  "'": 6, "(": 7, ")": 8, "*": 9, "+": 10, ",": 11,
  "-": 12, ".": 13, "/": 14, "0": 15, "1": 16, "2": 17,
  "3": 18, "4": 19, "5": 20, "6": 21, "7": 22, "8": 23,
  "9": 24, ":": 25, ";": 26, "<": 27, "=": 28, ">": 29,
  "?": 30, "@": 31, "A": 32, "B": 33, "C": 34, "D": 35,
  "E": 36, "F": 37, "G": 38, "H": 39, "I": 40, "J": 41,
  "K": 42}

polyN = ["A" * 18, "C" * 14, "T" * 18, "G" * 14]

#----------------------------------------------------------------------
def prod(factors):
  """perform the product of all elements in a list"""
  return functools.reduce(operator.mul, factors, 1)

#----------------------------------------------------------------------
def cwf(seq):
  """calculate a measure of complexity in for input sequence (orlov et al., NAR, 2004)"""
  N = len(seq)
  K = 4
  nucCount = [seq.count("A"), seq.count("T"), seq.count("G"), seq.count("C")]
  prodValue = prod([math.factorial(x) for x in nucCount])

  cwf = (1 / N) * math.log( (math.factorial(N) / prodValue), 4)
  return cwf

#----------------------------------------------------------------------
def polyTrimming(seq, quals):
  """trims everything that follows [18,14,18,14] stretchces of [A,C,T,G]"""
  matches = []
  for stretch in polyN:
    matches.append(seq.find(stretch))

  limit = min([x for x in matches if x >= 0], default = len(seq))
  return seq[:limit], quals[:limit]

#----------------------------------------------------------------------
def compTrimming(seq, quals, window, Q):
  """calculate a measure of complexity for a long sequence."""
  #checking input
  Q = float(Q)

  #calculating complexity vector
  compList = []
  for iterator in range(len(seq) - window + 1):
    compList.append(cwf(seq[iterator:iterator+window]))

  #calculating pos of maximum value and position where values <= 0
  maxPos = [0, -50]
  negativePos = 0
  for position, complexity in reversed(list(enumerate(compList))):
    value = sum([Q - x for x in compList[position:]])
    if value > maxPos[1]:
      maxPos[0] = position
      maxPos[1] = value
    if value <= 0:
      negativePos = position
      break

  if len(compList) == negativePos + 1 or len(compList) == maxPos[0] + 1:
    return seq, quals
  else:
    compLimit = max(negativePos, maxPos[0])
    return seq[:compLimit+1], quals[:compLimit+1]


#----------------------------------------------------------------------
def qualTrimming(seq, quals, Q):
  """Trim sequence and quality string based on quality"""
  #checking input
  Q = int(Q)
  qualList = [qualityDict[x] for x in quals]

  #calculating pos of maximum value and position where values <= 0
  maxPos = [0, -50]
  negativePos = 0
  for position, quality in reversed(list(enumerate(qualList))):
    value = sum([Q - x for x in qualList[position:]])
    if value > maxPos[1]:
      maxPos[0] = position
      maxPos[1] = value
    if value <= 0:
      negativePos = position
      break

  qualLimit = max(negativePos, maxPos[0])
  return seq[:qualLimit+1], quals[:qualLimit+1]


#argument parsing
options = argparse.ArgumentParser(description = "Trim a fastq file from the 3' end according to quality and complexity",
                                  formatter_class=argparse.ArgumentDefaultsHelpFormatter)
options.add_argument("fastqFile", help = 'The path to the fastq file to be trimmed')
## options.add_argument("trimmedFile", help = 'The path to the file where the trimmed fastq will be written')
options.add_argument("-x", '--complexityThreshold',
                     help = 'The sequence complexity threshold below which a window will be considered for trimming.',
                     action = "store",
                     default = '0.35',
                     type = float)
options.add_argument('-q', '--qualityThreshold',
                     help = 'The quality threshold below which a window will be considered for trimming',
                     action = 'store',
                     default = 25,
                     type = int)
options.add_argument("-w", "--window",
                     help = 'length of the window that will be used to calculate complexities and qualities.',
                     action = 'store',
                     default = 5,
                     type = int)
options.add_argument('-v', '--verbose',
                     help = 'Enable logging messages for every read to standard error. WARNING: very verbose, with small --limit values',
                     action = 'store_true')
options.add_argument('-s', '--skipOut',
                     help = 'Save reads trimmed to a length < minLength (see that option) to the specified file (*.fastq.gz format)',
                     type = str)
options.add_argument('--minLength',
                     help = 'specify minimum length (shorter reads are discarded)',
                     type = int,
                     default = 20)
options.add_argument('--maxLength',
                     help = "cut reads down to this length (from 3'-end, to simulate shorter sequencing)",
                     type = int,
                     default = 10000)
args = options.parse_args()

# before we do anything, report which node we're running on:
var = 'HOSTNAME'
if( var in os.environ) :
    ePrint("Running on node " + os.environ[var] +"\n")
else:
    ePrint("Environment variable "
          + var + " undefined, can't determine which node we're running on\n")
## OK, we could do a systemcall to hostname (1)

#variable definition
fastq = open(args.fastqFile, 'r')
compThreshold = args.complexityThreshold
qualThreshold = args.qualityThreshold
window = args.window

if args.skipOut:
  skipOut = gzip.open(args.skipOut, "wt")

counts = {'seen':0, 'poly':0, 'quality':0, 'complexity':0, 'survive':0 }

#trimming
with sys.stdout as trimFile:
  for line in fastq:
    #extracting relevant lines
    name = line.strip()
    seq = next(fastq).strip()
    plus = next(fastq).strip()
    quals = next(fastq).strip()

    counts['seen'] +=1

    origSeq = seq
    origQuals = quals

    #poly trimming
    seq, quals = polyTrimming(seq, quals)

    if len(seq) < args.minLength:
      counts['poly'] +=1
      if args.skipOut:
        print("\n".join([name+":badpoly", origSeq, plus, origQuals]), file= skipOut)
        continue
      else:
        continue

    #quality trimming
    seq, quals = qualTrimming(seq, quals, qualThreshold)

    if len(seq) < args.minLength:
      counts['quality'] +=1
      if args.skipOut:
        print("\n".join([name+":badqual", origSeq, plus, origQuals]), file= skipOut)
        continue
      else:
        continue

    #complexity trimming
    seq, quals = compTrimming(seq, quals, window, compThreshold)

    if len(seq) < args.minLength:
      counts['complexity'] +=1
      if args.skipOut:
        print("\n".join([name+":badcmplxty", origSeq, plus, origQuals]), file= skipOut)
        continue
      else:
        continue

    #printing trimmed block
    counts['survive'] +=1
    print("\n".join([name, seq[:args.maxLength], plus, quals[:args.maxLength] ]), file= trimFile)

if args.skipOut:
  skipOut.close()

contents = {'compTrim' : counts}
writeDelimitedJSON(contents)

#important line, ensures multitrim that the job completed successfully
ePrint("Successfully Completed")

