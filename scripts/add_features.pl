#!/usr/bin/env perl
my $usage = "Usage: $0 --bam file.bam --prot FILE.gtf args ...

Script to assign reads from a BAM file to genomic features from a number
of GTF files (using output from featureCounts, part of the subread
package), adding them to the original BAM file. The original BAM file is
deleted unless --keep is specified). The script assumes SAM format as
output by the STAR aligner (this is checked during parsing).

The featureCounts information is added to the SAM lines using exactly
5 optional tags (after the STAR tags NH:i:x, HI:ix, AS:i:x and NM:i:x)
The added tags are 

  ps:Z:x   protein_coding, sense direction
  cl:Z:x   protein_coding clusters (sense)
  al:Z:x   antisense + lincRNA (senes)
  re:Z:x   rest (any other features)
  pa:Z:x   protein_coding, antisense direction

The 'x' are either 'multi' (for multimappers; no further assignment is
done because featureCounts doesn't do it); 'unk' (in case it is not
assigned to that type); ambigN (when several features correspond to this
location; N is the multiplicity), or lastly, and hopefully, a gene
identifier.  Assignments to a type are not exclusive, and there is no
precedence in the assignments (that is done later by the tally.pl
script). If the read was unmapped (SAM flag 0x4, refseq name '*'), no
assignments are reported (note that STAR in that case also outputs an
undocumented uT:A tag!).

The input bam file is renamed to itsname-RAW.bam in its original location
(in order to avoid potential overwriting). The resulting bam file will be
written to the current directory (the original location is ignored).

Mandatory arguments:

  --bam FILE.bam            input file with reads
  --prot FILE.gtf           GTF file with protein_coding features to assign the reads to. 
                            They will be assigned ps:Z:XXX with the status from the protein
Optional:

  --clus FILE.gtf           GTF file with protein_coding clusters (see cluster-GTFprotein_coding.R)
  --anti_lincRNA FILE.gtf   GTF file with antisense and lincRNA features
  --rest FILE.gtf           GTF file with any other features ADD
  --dryrun                  Show what would be done, but don't 
  --keep                    Don't delete the input bam file nor *.featureCounts files 
                            after finishing
Debugging:
  --skipFC                  skip running the featureCounts, only integrate the 
 			    existing *.featureCounts files into the BAM file

(At some point, someone should parallelize this thing)

Written by <plijnzaad\@gmail.com>
";


use strict;
use File::Basename;
use FileHandle;
use Getopt::Long;

use tools;
my @argv_copy = @ARGV;
my $version=getversion($0);
my $script=basename($0);

my($bam, 
   $prot, $clus, $anti_lincRNA, $rest,
   $dryrun, $keep, $skipFC, $help);

die $usage unless GetOptions(
  "b|bam=s"	=> \$bam,
  "prot=s"	=> \$prot,
  "clus=s"	=> \$clus,
  "anti_lincRNA=s"	=> \$anti_lincRNA,
  "rest=s"	=> \$rest,
  "n|dryrun"	=> \$dryrun,
  "k|keep"	=> \$keep,
  "h|help"	=> \$help,
  "skipFC"      => \$skipFC
    );

die $usage if ($help || !$bam || !$prot);

# GTF files to use for each of the feature classes:
my $featuretypes = { prot => $prot,
               prot_as => $prot,
               clus=>$clus,
               anti_lincRNA => $anti_lincRNA, 
               rest => $rest };

my $featuretypecode = { prot => 'ps',
                  prot_as => 'pa',
                  clus=>  => 'cl',
                  anti_lincRNA => 'al',
                  rest => 're' };

my @featuretypes = qw(prot clus anti_lincRNA rest prot_as);

die "$bam: input must have .bam extension" unless $bam =~ /\.bam$/;

{ my @featuretypes0 = @featuretypes;
  @featuretypes=();                           # filter out the ones that are not used
  for my $featuretype (@featuretypes0 )  { 
    my $gtf= $featuretypes->{$featuretype};
    if ( $gtf ) { 
      die "$gtf: input must have .gtf extension" unless $gtf =~ /\.gtf$/;
      die "$gtf: $! " unless -f $gtf;
      push(@featuretypes, $featuretype);
    } else {
      warn "No file specified for feature type '$featuretype', will not be counted,";
    }
  }
}

checkBinaries();

## rename:
my ($name, $inPath, $suffix)=fileparse($bam, '.bam');
$inPath = '.' unless $inPath;
$inPath =~ s|/$||;

my $outPath = '.';                      # use hardcoded variable now, may change later

my $rawbam="$inPath/$name-RAW.bam";

my $len=length($rawbam);
if ($len > 250) {
  warn "Filename length of input bam is too long: $len\n";
  warn "Keep it below 250 or so, it gives untraceable problems with featureCounts 1.5.2\n";
  die "Full pathname used is \n$rawbam\n";
}

warn "Renaming $bam to $rawbam";
my $outbam="$outPath/$name-OUT.bam";
warn "Will write output to  $outbam (will be renamed afterwards)";

my $cmd="mv -n $bam $rawbam";           # no clobber
$cmd ="echo DRYRUN: '$cmd'" if $dryrun;
my $status=0;
$status=execute($cmd);

die "Aborting" unless $status==0;

## run featureCounts a number of times

my $featCounts={};                      # 

### run featureCounts for the different GTF files:
FEATURETYPE:
for my $featuretype ( @featuretypes )  { 
  my $gtf = $featuretypes->{$featuretype};
  my $out="$outPath/$name-$featuretype.txt";
  my $sensecode = $featuretype eq 'prot_as' ? 2 : 1;
  $cmd ="featureCounts -s $sensecode -a $gtf -t exon -R -o $out $rawbam > $out.log 2>&1 ";
  ## If all is well this includes the UTRs, but it depends of course
  ## on how the GTF file was constructed. EnsEMBL/GenCode should be fine
  $cmd ="echo DRYRUN: '$cmd'" if $dryrun;
  $cmd ="echo SKIPPING: '$cmd'" if $skipFC;
  $status=execute($cmd);
  $status=0;
  die "Aborting" unless $status==0;
  
  ## the counts files can be discarded since they don't have well information
  if (!$keep && !$skipFC ) { 
    $cmd ="rm $out"; # there's also $out.summary but that is small and maybe useful
    $cmd ="echo DRYRUN: '$cmd'" if $dryrun;
    $status=execute($cmd);
    die "Aborting" unless $status==0;
  }
  ## we need the $rawbam.featureCounts file, but that will be overwritten, so rename
  $out="$outPath/$name-$featuretype.featureCounts";
  my $rawbamFeatureCounts = "$outPath/$name-RAW.bam.featureCounts";
  $featCounts->{$featuretype}->{file}=$out;
  $cmd="mv -n $rawbamFeatureCounts $out";
  $cmd ="echo DRYRUN: '$cmd'" if $dryrun;
  $cmd ="echo SKIPPING: '$cmd'" if $skipFC;
  $status=execute($cmd);
  $status=0;
  die "Aborting" unless $status==0;
}                                       # FEATURETYPE

if ($dryrun) { 
  warn "No further sub-commands, exiting\n";
  exit 0;
}

## now integrate; first write header:

## open rawbam and the featurecount files:
my $rawbampipe = "samtools view $rawbam | ";
open(RAWBAM, $rawbampipe) || die "$rawbampipe: $!";

my $outpipe="  | samtools view -b -o $outbam";
open(OUTBAM, $outpipe) || die "$outpipe: $!";

write_header();

for my $featuretype ( @featuretypes ) { 
  my $file=$featCounts->{$featuretype}->{file};
  my $fh=FileHandle->new;
  $fh->open($file) || die "$file: $!";
  $featCounts->{$featuretype}->{fh}=$fh;
}

### The order in the bam file and in the featureCounts files is identical
### (and we check this during parsing), so the merging is easy
READ:
while(<RAWBAM>) {
  chomp;
  my @fields = split("\t",$_);
  my ($QNAME,$FLAG,$RNAME,$POS,$MAPQ,$CIGAR,$MRNM,$MPOS,$ISIZE,$SEQ,$QUAL,@rest)=@fields;

  my @codes=();
  for my $featuretype ( @featuretypes ) {
    my $fh=$featCounts->{$featuretype}->{fh};
    my $line=<$fh>;
    my ($id, $type, $featname, $total)=split("\t", $line);
    die "$featCounts->{$featuretype}->{file}: expected readId $QNAME, got $id (line $.)" unless $QNAME eq $id;
    my $code=$featuretypecode->{$featuretype};
    my $fc;
    if ($type eq 'Unassigned_Unmapped') {
      $fc='unmapped';
    } elsif ($type eq 'Assigned') {
      $fc="$code:Z:$featname";
    } elsif ($type eq 'Unassigned_NoFeatures') { 
      $fc="$code:Z:!unk";
    } elsif ($type eq 'Unassigned_Ambiguity') { 
      die "wrong format: $line" unless $total =~ /Number_Of_Overlapped_Genes=(\d+)/;
      $fc="$code:Z:!ambig$1";
    } elsif ($type eq 'Unassigned_MultiMapping') { 
      $fc="$code:Z:!multi";
    } else {
      die "Unknown featureCounts type: $type" unless $type;
    }
    push(@codes, $fc) unless $fc eq 'unmapped';
  }                                     # for $featuretype
  print OUTBAM join("\t",(@fields, @codes)) . "\n";
}                                       # READ

close(RAWBAM) || die "$rawbampipe: $!";
close(OUTBAM) || die "$outpipe: $!";

for my $featuretype ( @featuretypes ) { 
  $featCounts->{$featuretype}->{fh}->close() || die "$featCounts->{$featuretype}->{file}: $!";
}

if (! $keep ) { #this is probably broken by the new changes i made
  for my $featuretype (@featuretypes) { 
    $cmd="rm $rawbam $outPath/$name-$featuretype.featureCounts";
    $cmd ="echo DRYRUN: '$cmd'" if $dryrun;
    my $status=execute($cmd);
    die "Aborting" unless $status==0;
  }
}

my $finalBam = "$outPath/$name.bam";
$cmd = "mv -n $outbam $finalBam"; 
$cmd ="echo DRYRUN: '$cmd'" if $dryrun;
$status=execute($cmd);
$status=0;
die "Aborting" unless $status==0;

my $hash=count_features();
$hash->{'prot_gtf'} = $prot;
write_json({addFeatures => $hash });

warn "\nSuccessfully Completed";

# ------------------------------------------------------------------------

sub checkBinaries { 
  my $cmd="featureCounts -v 2>&1 | grep 'featureCounts v1.5' 2>&1 ";
  my $status=execute($cmd);
  die "featureCounts (part of the subread package) could not be found or has wrong version" unless $status==0;
}                                       # checkBinaries

sub write_header {
  my $headerpipe = "samtools view -H $rawbam | ";
  open(HEADER, $headerpipe) || die "$headerpipe: $!";
  my $star=undef;
  while(<HEADER>) { 
    $star++ if /^\@PG\tID:STAR/;
    print OUTBAM $_;
  }
  die "Input BAM file was not produced by STAR, cannot use this" unless $star;

## additional header:
  print OUTBAM "\@PG\tID:add_features\tPN:add_features.pl\tVN:$version\tCL:$script @argv_copy\n";
  close(HEADER) || die "$headerpipe: $!";
}                                       # write_header

## found out how many features were found per featuretype
sub count_features { 
  my $nfeatures={};
FEATURETYPE:
  for my $featuretype ( @featuretypes )  { 
    my $file = "$outPath/$name-$featuretype.txt.summary";
    open(SUM, $file) || die "$file: $!";
    while(<SUM>) { 
      next unless /^Assigned\s+(\d+)$/;
      $nfeatures->{$featuretype} = $1;
    }
    die "Did not find number of assigned features for feature type $featuretype in file $file"
        unless defined ($nfeatures->{$featuretype});
    close(SUM) || die "$file: $!";
  }
  $nfeatures;
}
