#!/bin/sh
# -*- mode: sh; -*-
## wrapper script for STAR that makes use of the proper local
## $TMPDIR on the compute node. This script relies on STAR being on
## $PATH (use the lmod system for that)

warn(){ 
    echo "$@" 1>&2 
}

die(){ 
    warn "$@"
    exit 28 
}

if [ "$TMPDIR" == "" -o ! -d "$TMPDIR" ]; then
    die "$0: TMPDIR '$TMPDIR' is not defined or not a directory (mebbe request -l tmpspace=nG during job submission?)  "
fi

for arg in "$@"; do 
    if [ "$arg" == '--outTmpDir' ]; then
        die "$0: don not pass the --outTmpDir argument to STAR.sh, I take are of that!"
    fi
done

tmpdir=$TMPDIR/STARtmp # must not yet exist!

if [ -d $tmpdir ] ; then
    die "$0: tmpdir $tmpdir exists, STAR does not want that ..."
fi
warn "Using $tmpdir"

STAR  "$@" --outTmpDir $tmpdir
