#!/bin/sh
# -*- mode: sh; -*-

###
module load Sharq/1

dir=/hpc/pmc_holstege/sc-facility/sortseq/preprocessing/test456
mkdir $dir
cd $dir

for read in 1 2 ; do
    nice zcat ../../rawdata/PMC-TM-241_HVFY2BGXB_S7_L001_R${read}_001.fastq.gz \
        | head -1000000 | nice gzip -n  > test_L1_R$read.fastq.gz
done

### see if we get any time at all:
## Sharq.py --species human -j Sharq.plateDiagnostics.empties=col24 -j Sharq.preProcessFastq.script=foo  test

### invoke with old script
module load Sharq/1
Sharq.py --species human test


for i in *z; do
    ln -s $i  ${i/test/test_new}
done

module load Sharq/2
Sharq.py --species human test_new
