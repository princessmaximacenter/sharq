workflow Sharq {
    String sampleName
    String inputPattern

    call getFileNames {input: inputPattern = inputPattern}
    scatter (laneFile in getFileNames.allLaneFiles) {
        call preProcessFastq { input: inFile = laneFile,
                                      sampleName = sampleName }
    }
    call gatherAndSplitCBC  { input: CBCs = preProcessFastq.out,
                                     sampleName=sampleName }
    scatter ( cbc in gatherAndSplitCBC.splits ) { 
       call compTrim {input: inFile = cbc , sampleName = sampleName }
    }
    call gatherCompTrim {input: CBCs = compTrim.out,
    				tooshorts = compTrim.tooshort,
    				logs = compTrim.log,
                                sampleName=sampleName }
    call STAR {input: inFile = gatherCompTrim.out, sampleName = sampleName}
    call addFeatures {input: inFile = STAR.out, sampleName = sampleName}
    call processSAM {input: inFile = addFeatures.out, sampleName = sampleName}
    call collectFiles {input: procSAMOutputs = processSAM.out,
                             sampleName = sampleName,
                             compTrimLog = gatherCompTrim.log,
                             STARLog = STAR.log,
                             addFeaturesLog = addFeatures.log,
                             processSAMLog = processSAM.log
                             # processSAMGPout=processSAM.GPout # add later if needed
                         }
    call plateDiagnostics {input: inFile = collectFiles.out, sampleName = sampleName}
    call cleanup {input: procSAMOutputs = processSAM.out,
                          sampleName = sampleName,
                          compTrimLog = gatherCompTrim.log,
                          compTrimTooshort = gatherCompTrim.tooshort,
                          STARLog = STAR.log,
                          STARLogFinalOut = STAR.LogFinalOut,
                          addFeaturesLog = addFeatures.log,
                          processSAMLog = processSAM.log,
                          processSAMGPout = processSAM.GPout,
                          processedBam = addFeatures.out,
                          plateDiagPdf = plateDiagnostics.out,
                          plateDiagWarnings = plateDiagnostics.warnings,
                          plateDiagStats = plateDiagnostics.stats,
                          plateDiagLive = plateDiagnostics.live,
                          plateDiagPerwellfits = plateDiagnostics.perwellfits,
                          plateDiagLog = plateDiagnostics.log,

}
}                                       # Sharq
                          
task getFileNames { # find the *.gz files to process
    String inputPattern # path/to/*R1.fastq.gz

    #hpc parametes
    String tmpSpace
    String cpu
    String memory
    String time

  command <<<
        set -euo pipefail
        newgrp pmc_holstege
        umask 002 # -rw-rw-r--

        echo ${inputPattern} 1>&2
        if [ -z  "`\ls ${inputPattern}`" ]; then
            echo "Expected to find files looking like ${inputPattern}" 1>&2
            exit 12
        fi
        echo ${inputPattern} | tr ' ' '\n' \
        | while read one ; do
            two=$(echo $one | sed 's/R1.fastq.gz$/R2.fastq.gz/')
            if [ -z  "`\ls $two`" ]; then
                echo "Expected to find file $two" 1>&2
                exit 13
            fi
            echo -e "$one,$two"
        done
    >>>

    output {
        Array[String] allLaneFiles = read_lines(stdout())
    }

    runtime {
        cpu: "${cpu}" # 1
        memory: "${memory}" # 1 GB
        wallclock: "${time}" # 00:00:00
        tmpspace: "${tmpSpace}" # 1G
    }
}                                       # getFilenames

task preProcessFastq { # adds UMI and well barcode to the read2's
    String sampleName
    String inFile
    
    # from json:
    String script
    String umiPos
    String cbcPos

    #hpc parametes
    String tmpSpace
    String cpu
    String memory
    String time

  command <<<
        set -euo pipefail
        newgrp pmc_holstege
        umask 002 # -rw-rw-r--
        
        ${script} \
        --fastq ${inFile} \
        --umipos ${umiPos} \
        --cbcpos ${cbcPos} \
        2> ${sampleName}.preProcessFastq.log \
        | gzip -n \
        >  ${sampleName}_cbc.fastq.gz
        sleep 5
    >>>

    output {
        File out = "${sampleName}_cbc.fastq.gz"
        File log = "${sampleName}.preProcessFastq.log"
    }

    runtime {
        cpu: "${cpu}" # 1
        memory: "${memory}" # 1 GB
        wallclock: "${time}" # 00:00:00
        tmpspace: "${tmpSpace}" # 1G
    }
}                                       # preProcessFastq

task gatherAndSplitCBC { # concats 4 lanes and splits them for parallel complexityTrimming
  String sampleName
  Array[File] CBCs
  
  String lines  # Sharq.gatherAndSplitCBC.lines json
  # inputs from json (HPC parameters)
  String tmpSpace
  String cpu
  String memory
  String time

  command <<<
    set -euo pipefail
    newgrp pmc_holstege
    umask 002 # -rw-rw-r--
    
    zcat ${sep=' ' CBCs} | split -l ${lines} --suffix-length=4 --additional-suffix=.fastq - ${sampleName}.split.
  >>>

  output {
      Array[File] splits = glob("${sampleName}.split.*.fastq")
  }

  runtime {
    cpu: "${cpu}" # 1
    memory: "${memory}" # 1 GB
    wallclock: "${time}" # 00:00:00
    tmpspace: "${tmpSpace}" # 1G
  }
} # gatherAndSplitCBC

task compTrim { # complexity trimmming
    String sampleName
    File inFile

    # from json
    String script
    String compThreshold
    String qualThreshold
    String window
    String minLength
    String maxLength

    #hpc parametes
    String tmpSpace
    String cpu
    String memory
    String time

    String tooshortfile = sampleName + "-compTrim-tooshort.fastq.gz"
    String comptrimmed = sampleName +  "-compTrim.fastq.gz"
    String logfile = sampleName +  ".compTrim.log"

  command <<<
        set -euo pipefail
        newgrp pmc_holstege
        umask 002 # -rw-rw-r--
        
        ${script} \
        --minLength ${minLength} \
        --complexityThreshold ${compThreshold} \
        --qualityThreshold ${qualThreshold} \
        --window ${window} \
        --maxLength ${maxLength} \
        --skipOut ${tooshortfile} \
        ${inFile}  2> ${logfile} | gzip -n > ${comptrimmed}
        sync
        sleep 5
    >>>

    output {
        File out = comptrimmed
        File tooshort = tooshortfile
        File log = logfile
    }

    runtime {
        cpu: "${cpu}" # 1
        memory: "${memory}" # 1 GB
        wallclock: "${time}" # 00:00:00
        tmpspace: "${tmpSpace}" # 1G
    }
}                                       # compTrim

task gatherCompTrim { # concatenates the trimmed files back into big fastq file
  String sampleName
  Array[File] CBCs
  Array[File] tooshorts
  Array[File] logs
    
  # inputs from json (HPC parameters)
  String tmpSpace
  String cpu
  String memory
  String time

  String cbc_final = sampleName + "-compTrim.fastq.gz"
  String tooshort_final = sampleName + "-compTrim-tooshort.fastq.gz"
  String log_final = sampleName + ".compTrim.log"
  
  command <<<
    set -euo pipefail
    newgrp pmc_holstege
    umask 002 # -rw-rw-r--

    cat ${sep=' ' CBCs} >  ${cbc_final}
    cat ${sep=' ' tooshorts} > ${tooshort_final}
    merge-compTrim-logs.pl ${sep=' ' logs} > ${log_final} # adds totals
  >>>

  output {
    File out = cbc_final
    File tooshort = tooshort_final
    File log = log_final
  }

  runtime {
    cpu: "${cpu}" # 1
    memory: "${memory}" # 1 GB
    wallclock: "${time}" # 00:00:00
    tmpspace: "${tmpSpace}" # 1G
  }
} # gatherCompTrim

task STAR { # do mapping
    #global variables
    String sampleName

    #task parameters
    File inFile
    String script
    String genomeDir
    String runThreadN
    String twopassMode
    String alignEndsType
    String alignSoftClipAtReferenceEnds
    String outSAMunmapped
    String readFilesCommand
    String outStd
    String outSAMtype
    String minNovelJunctOverhang

    #hpc parametes
    String tmpSpace
    String cpu
    String memory
    String time

  command <<<
        set -euo pipefail
        newgrp pmc_holstege
        umask 002 # -rw-rw-r--
    ## note: runThreadN should be 2*${cpu}, but we can't do arithmetic inside WDL ...
        ${script} \
        --genomeDir ${genomeDir} \
        --runThreadN ${runThreadN} \
        --twopassMode ${twopassMode} \
        --alignEndsType ${alignEndsType} \
        --alignSoftClipAtReferenceEnds ${alignSoftClipAtReferenceEnds} \
        --outSAMunmapped ${outSAMunmapped} \
        --alignSJoverhangMin ${minNovelJunctOverhang} \
	--readFilesCommand ${readFilesCommand} \
        --outStd ${outStd} \
        --outSAMtype ${outSAMtype} \
        --readFilesIn ${inFile}\
        > ${sampleName}.bam \
        2> ${sampleName}.STAR.log;
        (echo '## -------- JSON START -------- ';
        awk '/^genomeFastaFiles.*RE-DEFINED/{print "{\"STAR\":{\"genome\":\""$2"\"}}"}' Log.out;
        echo '## -------- JSON END -------- ' ) >> ${sampleName}.STAR.log;
        mv Log.final.out "${sampleName}.STARLog.final.out"
        chmod -vR g+rw *
        find . -type d -exec chmod -v r+xs {} \;
    >>>

    output {
        File out = "${sampleName}.bam"
        File log = "${sampleName}.STAR.log"
        File LogFinalOut = "${sampleName}.STARLog.final.out"
    }

    runtime {
        cpu: "${cpu}" # 1
        memory: "${memory}" # 1 GB
        wallclock: "${time}" # 00:00:00
        tmpspace: "${tmpSpace}" # 1G
    }
}                                       # STAR

task addFeatures {  # add gene names (etc) to the reads by combining genomics coords with GTF
    #global variables
    String sampleName

    #task parameters
    File inFile
    String script
    File prot
    File clus
    File antiLincRNA
    File rest


    #hpc parametes
    String tmpSpace
    String cpu
    String memory
    String time

  command <<<
        set -euo pipefail
        newgrp pmc_holstege
        umask 002 # -rw-rw-r--
	module load Sharq;

        ### NOTE: add_features.pl calls featureCounts, which has a weird and unclear upper
        ### limit on path name length (318 is too long). It crashes without a proper warning
        ### or indication of what went wrong $%^&*
        ${script} \
        --keep \
        --bam ${inFile} \
        --prot ${prot} \
        --clus ${clus} \
        --anti_lincRNA ${antiLincRNA} \
        --rest ${rest} \
        > ${sampleName}-assign.out \
        2> ${sampleName}.addFeatures.log
    >>>
    output {
        File out = "${sampleName}.bam"
        File log = "${sampleName}.addFeatures.log"
    }
    runtime {
        cpu: "${cpu}" # 1
        memory: "${memory}" # 1 GB
        wallclock: "${time}" # 00:00:00
        tmpspace: "${tmpSpace}" # 1G
    }
}                                       # addFeatures

task processSAM { # generate the count tables from the bam
    #global variables
    String sampleName

    #task parameters
    File inFile
    String script
    File id2name
    String barcodeFile # not File, since it is looked for dynamically
    String allowMM

    #hpc parametes
    String tmpSpace
    String cpu
    String memory
    String time

    command <<<
        set -euo pipefail
        newgrp pmc_holstege
        umask 002 # -rw-rw-r--
        
        ${script} \
        --prefix ${sampleName} \
        --id2name ${id2name} \
        --bar ${barcodeFile} \
        -allow_mm ${allowMM} \
        ${inFile} \
        > ${sampleName}.out \
        2> ${sampleName}.processSAM.log
    
    ### als produce a file with wellwise genomic/proteincoding ratios:
    module load utilities # for tflip and tawk
    sed -n 's/GENEID/#well/;p;/^#multi/q' ${sampleName}.rawcounts.txt \
     | tflip | tawk -v plate=${sampleName} '!/^#/{print plate "_" $1, $9/($2+1) }' \
     > ${sampleName}-GPratio.txt
    
    >>>
    output {
        Array[File] out = glob("${sampleName}.{{as_,}{{raw,umi}counts,transcripts},saturation,wellsat.{genes,umis}}.txt")
        File GPout = "${sampleName}-GPratio.txt"
        File log = "${sampleName}.processSAM.log"
    }
    runtime {
        cpu: "${cpu}" # 1
        memory: "${memory}" # 1 GB
        wallclock: "${time}" # 00:00:00
        tmpspace: "${tmpSpace}" # 1G
    }
}                                       # processSAM

task collectFiles { # create a list with files needed by platediagnostics
    String sampleName

    #task parameters
    Array[File] procSAMOutputs
    File compTrimLog
    File STARLog
    File addFeaturesLog
    File processSAMLog

    #hpc parametes
    String tmpSpace
    String cpu
    String memory
    String time

    command <<<
        set -euo pipefail
        newgrp pmc_holstege
        umask 002 # -rw-rw-r--
        
        printf %"s\n" ${sep=' ' procSAMOutputs} \
        ${compTrimLog} \
        ${STARLog} \
        ${addFeaturesLog} \
        ${processSAMLog} \
        > ${sampleName}.plateDiagFiles.txt
    >>>

    output {
        File out = "${sampleName}.plateDiagFiles.txt"
    }

    runtime {
        cpu: "${cpu}" # 1
        memory: "${memory}" # 1 GB
        wallclock: "${time}" # 00:00:00
        tmpspace: "${tmpSpace}" # 1G
    }

}                                       # collectFiles

task plateDiagnostics { # some basic QC
    #global variables
    String sampleName

    #task parameters
    File inFile
    String script
    String empties
    String ignore

    #hpc parametes
    String tmpSpace
    String cpu
    String memory
    String time

    command <<<
        set -euo pipefail
        newgrp pmc_holstege
        umask 002 # -rw-rw-r--
        
        ## create empty files so cleanup task can move 'something' even if platediagnostics failed
        touch ${sampleName}.platediagnostics.pdf \
	      ${sampleName}.platediagnostics.warnings.txt \
	      ${sampleName}.platediagnostics.stats.txt \
	      ${sampleName}.platediagnostics.live.txt \
              ${sampleName}.platediagnostics.perwellfits.txt \
              ${sampleName}.platediagnostics.log ;
        
        env VERBOSE=TRUE ${script} \
          --verbose \
          --cromwell \
          --empties ${empties} \
	  --ignore ${ignore} \
          --plate ${sampleName} \
          --filesFrom ${inFile} \
          --pdf ${sampleName}.platediagnostics.pdf \
        > ${sampleName}.platediagnostics.log 2>&1
    >>>
    output {
        File out = "${sampleName}.platediagnostics.pdf"
        File warnings = "${sampleName}.platediagnostics.warnings.txt"
        File stats = "${sampleName}.platediagnostics.stats.txt"
        File live = "${sampleName}.platediagnostics.live.txt"
        File perwellfits = "${sampleName}.platediagnostics.perwellfits.txt"
        File log = "${sampleName}.platediagnostics.log"
    }


    runtime {
        cpu: "${cpu}" # 1
        memory: "${memory}" # 1 GB
        wallclock: "${time}" # 00:00:00
        tmpspace: "${tmpSpace}" # 1G
	continueOnReturnCode: [0, 1] #so it continues if pdf cannot create a pdf
    }
}                                       # plateDiagnostics

task cleanup { # move things to their final place
    #global variables
    String sampleName

    #task parameters
    Array[File] procSAMOutputs
    File compTrimLog
    File compTrimTooshort
    File STARLog
    File STARLogFinalOut
    File addFeaturesLog
    File processSAMLog
    File processSAMGPout
    File processedBam
    File plateDiagPdf
    File plateDiagWarnings
    File plateDiagStats
    File plateDiagLive
    File plateDiagPerwellfits
    File plateDiagLog
    String outFolder

    #hpc parametes
    String tmpSpace
    String cpu
    String memory
    String time

  command <<<
        set -euo pipefail
        newgrp pmc_holstege
        umask 002 # -rw-rw-r--
        dir="${outFolder}/${sampleName}"
        if [ -d $dir ]; then
          # resulst from previous run, rename by appending its creation date
          mv -f $dir $dir-$(date -r $dir +%Y-%m-%d_%H:%M)      # ( no trailing '/' ! )
        fi
        mkdir -p $dir
	for i in ${sep=' ' procSAMOutputs} \
        	 ${compTrimLog} \
        	 ${compTrimTooshort} \
        	 ${STARLog} \
        	 ${STARLogFinalOut} \
        	 ${addFeaturesLog} \
        	 ${processSAMLog} \
        	 ${processSAMGPout} \
        	 ${processedBam} \
        	 ${plateDiagPdf} \
        	 ${plateDiagWarnings} \
        	 ${plateDiagStats} \
        	 ${plateDiagLive} \
        	 ${plateDiagPerwellfits} \
        	 ${plateDiagLog}
	do
		if [ -s $i ]; then
			cp -p $i $dir/
		fi
	done
    >>>

    output {

    }

    runtime {
        cpu: "${cpu}" # 1
        memory: "${memory}" # 1 GB
        wallclock: "${time}" # 00:00:00
        tmpspace: "${tmpSpace}" # 1G
    }

}                                       # cleanup
