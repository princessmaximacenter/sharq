#!/usr/bin/perl -w
# for usage see below.
use strict;
use Carp;
use File::Basename;
use Cwd;
use FileHandle;
use Getopt::Long;
use Set::Scalar;
use List::Util 'sum';

use tools;
use mismatch;

our ($barfile, $umi_len, $allow_mm, $prefix, $id2name, $help);

$allow_mm=1;

my $usage = "
Script to demultiplex single-cell RNA-seq data from bam files and convert
read counts to transcripts counts.

Usage: $0 --barcodefile barcodes.csv --umi_len UMILENGTH \\
           [--prefix name ] [ -allow_mm=1 ] [ --id2name FILE ] file.bam [ file2.bam ... ]

Arguments: 

--barcodefile PATH  File with cell bar codes (format: id \\t sequence). If PATH is a name
                    with slashes (prolly starting with a slash, IOW it's an absolute path),
                    that path will be used. Otherwise, the assumed path is ../data/. 

file.bam ...        Name(s) of the bam file(s), typically from several lanes. If
                    more than one they are assumed to have the same header. The
                    readnames should contain wellbarcode (which may contain mismatches) 
                    and umi; the tags should contain annotations (more details below_
Options:
--help              This message
--allow_mm N        How many mismatches to allow in the cell barcodes (default: $allow_mm)
--prefix NAME       Prefix for the output files (NAME.rawcounts.txt etc.,see below)
                    Default is the name of the first bam file without extension
                    and lane number.
--id2name FILE      Convert gene ids (e.g. ENSG000nnnn) to gene names using
                    this mapping (format: gene_id \\t gene_name) The
                    output file will use geneid__genename in the output
                    (e.g. ENSG00000223972__DDX11L1)

The bam file is expected to have been aligned with STAR, with
feature-assignments added in using add_features.pl (this uses featureCount
from the subread package).

The feature types are assigned in the following order of precedence:
(search for 'PRECEDENCE OF ASSIGNMENT' below):
 - protein_coding; (ps:Z:x)
 - protein_coding_cluster; (cl:Z:x)
 - known antisense and Long Intergenic non-coding RNAs (al:Z:x)
 - all other GTF features (re:Z:x)
 - protein_coding in antisense direction; (pa:Z:x, used for checking the mapping)

The following output and files will be created:

<STDOUT> \t\t\t overal statistics.
<STDERR> \t\t\t warnings and informational messages; also JSON
\$prefix.saturation.txt \t\t readsprocessed,readsmapped,validreadsseen,genesseen,umisseen,txptsseen
\$prefix.wellsat.genes.txt \t readsprocessed,genesseen (per well)
\$prefix.wellsat.umis.txt \t readsprocessed,umisseen (per well)
\$prefix.rawcounts.txt \t\t geneid, readcounts (per well)
\$prefix.umicounts.txt \t\t geneid, umitcounts (per well)
\$prefix.transcripts.txt \t geneid, txptcounts (per well)
\$prefix.as_rawcounts.txt \t geneid, antisense readcounts (per well)
\$prefix.as_umicounts.txt \t geneid, antisense umitcounts (per well)
\$prefix.as_transcripts.txt \t geneid, antisense txptcounts (per well)

\$prefix.transcripts.txt is the main output for use in
downstream analysis. This and the output are typically fed 
to platediagnostics.R to obtain a qc report.

This script was previously (25 sep 2017) called process_sam_cel384v3.pl 
which was modified from process_sam_cel384v2.pl (22 jun 2017)
which in turn was based on the script by Dominic Gruen.

Written by <plijnzaad\@gmail.com>
";


my $version=getversion($0);
warn "Running $0, version $version\nwith arguments:\n @ARGV\n";

die $usage unless GetOptions('barcodefile=s'=> \$barfile,
                             'allow_mm=i'=> \$allow_mm,
                             'prefix=s' => \$prefix,
                             'id2name=s' => \$id2name,
                             'help|h' => \$help);
my @bams=@ARGV;

if ( $help ) {
  print $usage;
  exit 0;
}

if ( !(@bams && $barfile ) ) {
  die $usage;
}

my $idmap=undef; 
$idmap = read_map($id2name) if $id2name;

if($barfile !~ m|/| ) { 
  use File::Basename;
  use Cwd 'abs_path';
  my ($fullpath) = abs_path($0);
  my ($script,$dir) = fileparse($fullpath);
  $dir = abs_path("$dir/../data");
  die "Inferred directory '$dir' not found" unless -d $dir;
  $barfile = "$dir/$barfile";
  die "$barfile: file not found" unless -f $barfile;
}

my $barcodes_mixedcase = mismatch::readbarcodes($barfile);       ## eg. $h->{'AGCGtT') => 'M3'
my $barcodes = mismatch::mixedcase2upper($barcodes_mixedcase);   ## e.g. $h->{'AGCGTT') => 'M3'

my $maxumis;
{ my $umi_len=length((keys %$barcodes)[0]);
  $maxumis = 4 ** $umi_len;
}

sub byletterandnumber { # sort the barcodes by their ids (which may contain prefixes)
  my ($aa,$bb) = ($a,$b);
  $aa=$barcodes->{$aa}; 
  $bb=$barcodes->{$bb}; 
  my($re)= qr/([A-Za-z_]*)([0-9]+)/;
  my ($Sa, $Na) = ($aa =~ $re);
  my ($Sb, $Nb) = ($bb =~ $re);
  ($Sa cmp $Sb) || ($Na <=>  $Nb);
}

my @cbcs = sort byletterandnumber (keys %$barcodes); # e.g. 'AGCGTT' (but sorted by wellnumber)
my @wells = map { $barcodes->{$_} } @cbcs;           # e.g. 'B6'

$barcodes->{'UNK'} = 'UNK';
@cbcs = ('UNK', @cbcs);
@wells = ('UNK', @wells);

my $mismatch_REs=undef;

if ($allow_mm) { 
  $mismatch_REs = mismatch::convert2mismatchREs(barcodes=>$barcodes_mixedcase, allowed_mismatches =>$allow_mm);
}
$barcodes_mixedcase=undef;              # not used in remainder, delete to avoid confusion

my $nreads = 0;                         # everything
my $ninvalidUMI=0;                      # skipped
### There used to be code to deal with UMIs containing N's, but they are
### very rare and not worth rescuing given the complexity of it. They
### were removed after version v15 (14 march 2017)

## mismatched cell barcodes:
my $nrescued_CBC=0;

my $nusable=0;
my $nunmapped=0;

my $nrefgenes=0;
my $nERCCs=0;

my $tc = {};                            # big hash with $tc->{txptname}{well}{umi}; only for unique-sense mapping
my $as_tc = {};                         # same but for antisense

# read through sam file create a hash with all genes and cells and extract mapped reads into the hash
my $samtools = "samtools";                    # alternative: sambamba, might be faster

if (!$prefix) { 
  $prefix=$bams[0];
  $prefix =~ s/\.bam//;
  $prefix =~ s/[-_,.]L0*[1-4][-_,.]//g if @bams > 1;
  warn "using prefix $prefix\n";
}

for my $f (@bams) {
  die "$f: $!" unless $f; 
}
my $cmd = "($samtools view -H $bams[0]; " . join("; ", map { "samtools view $_" } @bams) . ")";

open(IN,"$cmd |") || die "$cmd: $!";
my $saturation    = "$prefix.saturation.txt";

open(SATURATION, "> $saturation") || die "$saturation: $!";
my @headers=qw(reads genes umis txpts);
print SATURATION "#" . join("\t", @headers) . "\n";
print SATURATION join("\t", (1) x int(@headers) )."\n";

## well saturation: open three files
my $wellsat_prefix    = "$prefix.wellsat";
my $wellsat_files={};

for my $type ( qw(genes umis)  ) {
  my $file="$wellsat_prefix.$type.txt";
  my $fh = FileHandle->new("> $file") || die "$file: $!";
  $wellsat_files->{$type}=$fh;
  print $fh "reads\t" . join("\t", @wells) . "\n";
}

my $sample_every = 10_000;             # write counts to the saturation table every this-many reads
my $genes_seen={};                     # cumulative
my $umis_seen={};                      # cumulative
my $wellwise_seen={};                  # keys are those of %$wellsat_files

sub umi_correction { 
  my($n, $maxumis)=@_;
  return 'NA' if $maxumis <= 1;
  $n= $maxumis - 0.5 if $n >= $maxumis; # exhaustion correction, otherwise Inf transcripts (now 36908.7 :-)
  sprintf("%.2f", -$maxumis*log(1 - ($n/$maxumis)));
}

my $seen_star=0;
my $seen_addf=0;

HEADERLINE:
while(<IN> ) {
  ## loop is shifted one readline so that final printing of SATURATION is not skipped
  if ( /^\@SQ/ ){
    my ($tag, $name, @rest)=split("\t", $_);
    if ($name =~ /^ERCC-0/) { $nERCCs++; } else { $nrefgenes++; }
    next HEADERLINE;
  }
  if ( /^\@PG/ ){
    warn "$0: found: $_";
    $seen_star++ if /CL:STAR/;
    $seen_addf++ if /CL:add_features.pl/;
    next HEADERLINE;
  }
  last HEADERLINE unless /^@/;
}
die "BAM input was not produced by STAR in conjuntion with add_features"
    unless $seen_star && $seen_addf;

my @tags = sort(qw(NH HI AS nM ps cl al re pa)); # the tags to expect in the BAM file
## tags NH, HI, AS, nM from STAR; ps, pa, cl, al $re from add_features.pl
## (HI, AS, nM are not used downstream)
my $alltags=join(',', @tags);           # for checking

my @classes=qw(prot clus as+linc other mito ercc anti genomic multi unmapped); # decreasing usefulness

## NOTE: the sum of these categories is larger than the number of reads in
## the fastq file (see STARs Log.final.out, 'Number of input reads') owing
## to the 'many' category (these reads have several BAM lines).  The sum
## of all but the 'unmapped' and 'multi' classes is slightly (< 0.1 %)
## less than 'Uniquely mapped reads number' in Log.final.out, unclear why.

@classes = map { "#$_"; } @classes;     # will be used as pseudo-IDs

READ:
while(1) { 
  chomp $_;
  my @r1 = split("\t",$_);
  my ($QNAME,$FLAG,$RNAME,$POS,$MAPQ,$CIGAR,$MRNM,$MPOS,$ISIZE,$SEQ,$QUAL,@rest)=@r1;
  die "expected 5 or 9 tag fields, found " . int(@rest) unless ( @rest == 5  || @rest == 9 );
  ## 5 fields for unmapped reads: NH, HI, AS, nM, and  uT:A:1 (!?!?) 
  ## 9 fields for mapped reads:   NH, HI, AS, nM by STAR , and ps, cl, al, re, pa added by add_features.pl
  $nreads++;

  my ($cbc,$umi);
  my(@parts)=split(':', $QNAME);
  for my $tag ( @parts ) { 
    $cbc= $1 if $tag =~ /cbc=([A-Z]+)/i;
    $umi= $1 if $tag =~ /umi=([A-Z]+)/i;
  }
  die "$0: could not find cbc= or umi= in id $QNAME of the files @bams " unless $cbc &&  $umi;

  if ($umi =~ /N/i) { # very rare (< 0.1%), not worth rescuing. See v15 for code that did.
    $ninvalidUMI++;
    next READ;
  }
  
  ### check that the tags follow the STAR + add_features.pl conventions (regardless of ordering)
  my $tags={};
  if  ($FLAG & 4) {
    $nunmapped++;
    $tags->{NH}=0;                      # unmapped, don't bother assigning the other tags (are different anyway)
  } else { 
    map { die "Invalid syntax for tag: $_ (read $QNAME, line $.)" 
              unless /([a-zA-Z]{2}):.:(\S+)/; $tags->{$1}=$2; } @rest;
    my $got=join(',',sort keys %$tags);
    die "Too many or too few tags, found $got, expected $alltags (read $QNAME, line $.)\n" .
        "(are add_features.pl and process_sam_cel384v3.pl from the same version?)" unless $got eq $alltags;
  }

  my($id, $class, $antisense)=(undef, undef, 0);

  ### PRECEDENCE OF ASSIGNMENT:

  ### Now assign, both the id (if any) and type. Use the fact that all real
  ### features have uppercase IDs (this is checked).  'ids' starting with '!' are
  ### for things like 'ambig', 'unknown', 'multi' (assigned in add_features.pl). 
  ### Anything that has an id (apart from
  ### class '#anti') is a 'usable' read.
  if    ( $tags->{NH} == 0 )  { $id=undef; $class='#unmapped';}
  elsif ( $tags->{NH} > 1 )  { $id=undef; $class='#multi'; } # NOTE: this counts hits, not reads! Use ' && $tags->{HI} ==1 ' to count reads
  elsif ( $tags->{ps} !~ /^!/ ) { $id=$tags->{ps}; $class='#prot'; } # protein coding, sense
  elsif ( $tags->{cl} !~ /^!/ ) { $id=$tags->{cl}; $class='#clus';} # protein coding cluster, sense
  elsif ( $tags->{al} !~ /^!/ ) { $id=$tags->{al}; $class='#as+linc';} # known antisense or long intergenicc non-coding
  elsif ( $tags->{re} !~ /^!/ ) { $id=$tags->{re}; $class='#other';}
  elsif ( $tags->{pa} !~ /^!/ ) { $id=$tags->{pa}; $antisense=1; $class='#anti'; } # unknown antisense
  else  { $id = undef; $class='#genomic'; } # i.e. we have coordinates, but no unequivocal feature known for that location
  $id =~ s/\.\d+$// if $id;                 # get rid of version number
  $id .= "__$idmap->{$id}" if (defined($id) && $idmap && defined($idmap->{$id}) ); # to recognize mito!

  ### After this, assign classes '#mito' and '#ercc'.  They have the same
  ### precedence as '#prot' and '#clus' since they're derived from
  ### it. (But they are usually discarded in downstream analysis, so are
  ### ranked lower in the output).
  if($class eq '#prot'  || $class eq '#clus') { 
    $class = '#ercc' if $id =~ /ERCC-/;
    $class = '#mito' if $id =~ /__MT-/i; # note: there are also clusters for mito, in that case mito 'wins'
  }

  if (! exists $barcodes->{$cbc} ) {
    if ( $allow_mm ) { 
      $cbc=mismatch::rescue($cbc, $mismatch_REs);      # gives back the barcode without mismatches (if it can be found)
      $nrescued_CBC += defined($cbc);
    } else {
      $cbc=undef;
    }
  }
  $cbc = 'UNK' unless $cbc;        # virtual well, simplifies the tallying

  $tc->{$class}{$cbc}{'all'}++;    # class totals. UMIs pointless here, therefore dummy id

  if ($class eq '#anti') { 
    $as_tc->{$id}{$cbc}{$umi}++;        # different tables, for QC purposes only
  } else {
    if($id) { 
      $tc->{$id}{$cbc}{$umi}++;         # main counting event!
      ## keep stats for the saturation logs (both per well and overall), 
      ## but discard ERCC's, mitochondrials and unknown wells. 
      if ($class ne '#ercc' && $class ne '#mito' & $cbc ne 'UNK' ) {
        $nusable++;                     # for reporting only
        $genes_seen->{$id}++ ;
        $umis_seen->{$id.$umi}++;
        $wellwise_seen->{'genes'}{$cbc}{$id}++;
        $wellwise_seen->{'umis'}{$cbc}{$id.$umi}++;
      }
    }
  }
} continue {
  ## saturation logging:
  if ($nreads % $sample_every == 0 || eof(IN) ) { 
    my $g=int(keys(%$genes_seen));
    my $u=int(keys(%$umis_seen));
    print SATURATION  join("\t", 
                           ($nreads, 
                            $g, 
                            $u,
                            umi_correction($u,$maxumis*$g))) . "\n";
    
    ## note: for the wellwise counts, only print $nreads; get the rest from the 
    ## overall saturation counts
    my $fh;

    my @genecounts = map { int keys %{$wellwise_seen->{'genes'}{$_}} } @cbcs;
    $fh=$wellsat_files->{'genes'};
    print $fh "$nreads\t" .  join("\t", @genecounts) .  "\n";

    my @umicounts = map { int keys %{$wellwise_seen->{'umis'}{$_}} } @cbcs;
    $fh=$wellsat_files->{'umis'};
    print $fh "$nreads\t" .  join("\t", @umicounts) .  "\n";

    ## NOTE: always sum(@genecounts) >> $g and sum(@umicounts) > $u, because
    ## summing the per-well numbers counts many genes and umis doubly (and is meaningless)
    
  }
  warn int($nreads/1_000_000) . " million reads processed\n" if $nreads % 1_000_000 == 0;
  last READ if eof(IN);
  $_ = <IN>;
}                                        # READ

close(IN) || die "$cmd: $!";
close(SATURATION) || die "$saturation: $!";
foreach my $type (keys %$wellsat_files ) {
  my $fh= $wellsat_files->{$type};
  $fh->close() || die "Well saturation file for type $type :$!";
}

## -------- keep track of the classes
my $expect = Set::Scalar->new(@classes);
my $got = Set::Scalar->new( keys %$tc );

if (! $expect == $got ) {
  my @diff= ($expect - $got)->members;
  warn "Didn't find the following classes: " . join(' ', @diff) if @diff;
  @diff= ($got - $expect)->members;
  warn "Found following extra classes: " . join(' ', @diff) if @diff;
}
warn "*** BAM file contained only mapped reads!\n" unless $nunmapped;

## --------  done with collating, now tally the result and print them simultaneously

my $outnames= {RAW => "$prefix.rawcounts.txt",
               UMIS => "$prefix.umicounts.txt",
               TXPTS  =>"$prefix.transcripts.txt", 
               RAW_AS  =>"$prefix.as_rawcounts.txt",
               UMIS_AS => "$prefix.as_umicounts.txt",
               TXPTS_AS  =>"$prefix.as_transcripts.txt"};

my @outnames=keys %$outnames;

my $outfiles={};

for my $name ( @outnames ) { 
  my $file=$outnames->{$name};
  my $fh=FileHandle->new(" > $file ") or die "$file: $!";
  $fh->print( "GENEID\t".join("\t", @wells)."\n");
  $outfiles->{$name}=$fh;
}
$outnames=undef;                        #  not needed anymore

my $results = print_counts($tc, 0);
print_counts($as_tc, 1);

for my $name ( @outnames ) { 
  $outfiles->{$name}->close() or die "when closing $name: $!";
}

# -------- over all statistics:

my $nnoCBC = sum map { ($tc->{$_} &&  $tc->{$_}{'UNK'}) ? $tc->{$_}{'UNK'}{'all'} : 0; } @classes;
my $total = $nreads - $ninvalidUMI;

my $counts={};
$counts->{'allreads'}=$nreads;             # total in input BAM
$counts->{'total'}=$total;
$counts->{'invalid_UMIs'}=$ninvalidUMI;  # completely ignored because invalid UMI
$counts->{'rescued_barcodes'}=$nrescued_CBC; 
$counts->{'unknown_barcodes'}=$nnoCBC;   # over all classes
$counts->{'usable'}=$nusable;           # uniquely sense-mapped, known CBC, no mito nor ERCC
$counts->{'unmapped'}=$nunmapped;       # not mapped at all
$counts->{'exhausted_UMIs'}=$results->{nexhausted_umis}; # previously saturated_umis

# order of printing:
my @counts=qw(allreads total usable unmapped unknown_barcodes rescued_barcodes invalidUMIs exhausted_umis); 

sub line { print '-' x 40 . "\n"; }

line;
print "Raw read counts (not mutually exclusive):\n";
line;

my $fmt="%-16s:\t%+12s (%3.0f%%)\n";

foreach my $h (@counts) {
  my $n=$counts->{$h};
  $n=0 unless defined($n);
  printf $fmt, $h, commafy($n), $n/$total*100;
}
line;

    
## -------- summarize the class totals

for my $class (@classes) {
  my $n= sum map { $_->{'all'}; } values %{$tc->{$class}};
  $n=0 unless defined($n);
  # 'all' is the single dummy UMI (dumi:-) for the class pseudo-id's
  my $name=$class; $name =~ s/^#/class_/; 
  $counts->{$name} = $n;
}

@classes = map {/#(.*)/;"class_$1";} @classes;   # rename

print "\nBy class\n(mutually exclusive, and including those in unknown wells)\n";
line;
my $ntot=0;
foreach my $cl (@classes) {
  my $name=$cl; $name =~ s/^#/class_/;
  my $n = $counts->{$cl};
  $n=0 unless defined($n);
  $ntot += $n;
  printf $fmt,  $cl, commafy($n), $n/$total*100;
}
line;

die "Numbers don't add up: expected $total, got $ntot" unless $ntot  == $total;

write_json( {'processSAM'=>$counts} );

warn "\nSuccessfully Completed";
exit 0;

### ------------------------------------------------------------------------

sub print_counts {
  ## main output routine
  my($tc, $as)=@_;
  my $sensesuffix= $as ? "_AS" : "" ;

  my $nvalid_reads = 0;
  my $nexhausted_umis=0;

  my @out=("RAW$sensesuffix", "UMIS$sensesuffix", "TXPTS$sensesuffix");

  my @genes= (@classes, sort(grep(!/^#/, keys %$tc))); # the classes need fixed order

GENE:
   foreach my $gene (@genes) {           # row names for all the outfiles
    my $rowname=$gene;
    for my $name ( @out ) {
      $outfiles->{$name}->print($rowname) unless  ( $gene =~ /^#/ && $name !~ /^RAW/);
    }
  WELL:
    foreach my $cbc (@cbcs) { 
      my $n = 0;                          # distinct UMIs for this gene+well
      my $rc = 0;                         # total reads for this gene+well
      my $umihash=$tc->{$gene}{$cbc};
      my @umis = keys %{$umihash};

    UMI:
      foreach my $umi (@umis) {
        my $reads=$tc->{$gene}{$cbc}{$umi};
        $n += ($reads > 0);
        $rc += $reads;
      }                                   # UMI
      $nexhausted_umis += ($n == $maxumis) unless ($cbc eq 'UNK'  or $gene =~ /^#/ ); 
      $n = $n - 0.5 if ($n == $maxumis);  # exhaustion correction, otherwise Inf transcripts (now 36908.7 :-)
      my $txpts = -log(1 - ($n/$maxumis)) * $maxumis; # although pointless for the class totals

      # to keep filesize small, only start printing decimals when deviating more than 0.1 from umicount:
      if ($txpts - $n >= 0.05 ) {
        $txpts = sprintf("%.1f",$txpts);
      } else {
        $txpts = $n;
      }
      
      $outfiles->{"RAW$sensesuffix"}->print("\t$rc");
      
      if  ( $gene !~ /^#/ ) {
        # pointless for class totals, needs to be summed in a separate pass ...
        $outfiles->{"UMIS$sensesuffix"}->print("\t$n");
        $outfiles->{"TXPTS$sensesuffix"}->print("\t$txpts");
      }
    }                                   # WELL
    for my $name ( @out ) { 
      $outfiles->{$name}->print("\n") unless ( $gene =~ /^#/ && $name !~ /^RAW/);
    }
  }                                     # GENE
  return {nexhausted_umis=>$nexhausted_umis};
}                                       # sub print_counts

