#!/usr/bin/env python
## 
##

import time
import argparse
import inspect
import sys
import os
import json
import tempfile
import subprocess
from TUtils import ePrint

def warn(*args, **kwargs):
    print(*args, file = sys.stderr, flush = True, **kwargs)

def die(status=17, *args, **kwargs):
    print(*args, file = sys.stderr, flush = True, **kwargs)
    sys.exit(status)

warn("running dev version")

#custom action for argparse to allow storage of key value pairs from command line arguments
jsonInputsReplacements = {}
class StoreNameValuePair(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        key, value = values.split('=')
        jsonInputsReplacements[key] = value
        setattr(namespace, self.dest, jsonInputsReplacements)


#----------------------------------------------------------------------
def jsonLoader(args):
    """load the specified json file to be used as input. If explicit --jsonFile argument is given,
that is loaded and dict returned; otherwise, the commonInputs.json is loaded, then adapted/
completed/by integrating the species-specific json file (eg. ../data/humanInputs.json ) into it."""
    if args.jsonFile:
        with open(args.jsonFile, 'r') as jsonFile:
            try:
                configJson = json.load(args.jsonFile)
            except json.decode.JSONDecodeError:
                raise ValueError('something wrong with JSON file ' + args.jsonFile  )
            else:
                return configJson
    else:
        dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) + '/../data/'
        commonJson = dir + 'commonInputs.json'
        with open(commonJson, 'r') as commonJsonFile:
            try:
                commonJson = json.load(commonJsonFile)
            except json.decode.JSONDecodeError:
                raise ValueError('something wrong with JSON file ' + commonJson )
        # empties
        emptiesJson = args.empties
        # species specific stuff: loads from species-dependent file
        speciesJson = dir + args.species + 'Inputs.json'
        with open(speciesJson, 'r') as speciesFile:
            try:
                speciesJson = json.load(speciesFile)
            except json.decode.JSONDecodeError:
                raise ValueError('something wrong with JSON file ' + speciesJson )
        for key,value in speciesJson.items():
            if (key in commonJson and commonJson[key] != value):
                ePrint(f"**** Overriding {key}: {commonJson[key]} => {value}")
                commonJson[key] = value
        return commonJson



#----------------------------------------------------------------------
def jsonModifier(json, args):
    """updates the loaded json template with the specified parameters"""
    #add input
    inputDir = os.path.abspath(args.inputDir)
    pattern = f'{args.sampleName}[-_]L[{args.lanes}][-_]R1.fastq.gz' # globbed inside inputDir
    inputPattern = os.path.join(inputDir, pattern)

    # add missing parameters:
    json['Sharq.inputPattern'] = inputPattern
    json['Sharq.sampleName'] = args.sampleName
    json['Sharq.plateDiagnostics.empties'] = args.empties

    #modify the rest
    if args.jsonOptions:
        for key, value in args.jsonOptions.items():
            if key not in json:
                raise KeyError(f'no such parameter {key} in the template json.')
            else:
                json[key] = value

    return json

#----------------------------------------------------------------------
def filloutJsonTemplate(args):
    """loads, modifies, integrates then dumps the provided input json files for use by cromwell.
Returns the name of the file containing the complete json"""
    #load and modify json
    jsonDict = jsonLoader(args)
    modJson = jsonModifier(jsonDict, args)
    filename = args.sampleName + '.json'
    ePrint(f"**** Creating {filename}")
    jsonFile = open(filename, mode='w')
    jsonFile.write(json.dumps(modJson))
    jsonFile.close()
    return filename

usage="""

Typical example, assuming that files TM666_L[1-4]_R[12].fastq.gz are present in the
*current* directory, with cells sorted such that column1 of the plate was left empty
you would start the pipeline as

$ Sharq.py --species human --empties col1 \\
           -j Sharq.compTrim.maxLength=75 \\
           -j Sharq.plateDiagnostics.ignore=OPodd \\
           TM666 > cromwell.out 2> cromwell.log  & 

(two JSON defaults are overriden here using the -j option).

The script looks for files looking like  NAME[-_]L[\\d+][-_]R[12].fastq.gz

The script fills out a JSON template with relevant values, saves this to sampleName.json
in the current directory, then submits it to the queue.

For inspiration on how to start several Sharq runs simultaneously,
see Sharq/miscUtilities/sharqFrenzy.sh

"""

options = argparse.ArgumentParser(usage=usage,
                                  description= 'Execute the Sharq pipeline using wdl',
                                  formatter_class = argparse.ArgumentDefaultsHelpFormatter)

## jsonGroup = options.add_mutually_exclusive_group(required = True)

options.add_argument('sampleName', help = 'Name of the sample being analysed.')

options.add_argument('--species', '-s',  required=True,
                     help = 'specify which species template to use (mouse, human, HsMm_combined, yeast).',
                     choices = ['mouse', 'human', 'HsMm_combined', 'yeast'])

magicIncantation = '''
$ echo 'library(SCutils);cat(names(SCutils::.known.wellsets())," ")' | R --vanilla
'''

options.add_argument('--empties', '-e', required=True,
                     help = 'specify which wells are left empty for QC purposes. For the most recent ' +
'version of valid shorthands, issue the following on the command:\n' + magicIncantation)

options.add_argument('--jsonFile',
                       help = 'specify your input json file instead of picking a template (this overrides everything else).')

options.add_argument('--jsonOptions', '-j',
                     help = ('provide key value pairs separated by an equal sign '
                             'to override defaults for the '
                             'currently selected json template.'),
                     action = StoreNameValuePair,)
options.add_argument('--lanes',
                     help = 'specify a range of lanes to be analysed (only those present will be taken)',
                     default = '1-8',
                     type = str)
options.add_argument('--inputDir',
                     help = 'specify the directory where input files can be found',
                     default = './',
                     type = str)

args = options.parse_args()

#####

if args.jsonFile is None:
    inputJson = filloutJsonTemplate(args)
else:
    inputJson = args.jsonFile


scriptDir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
wdlPath = scriptDir + '/Sharq.wdl'

# cromwellConfig = '/hpc/local/CentOS7/gen/software/wdl-28.0.12/application.conf.hpc'
cromwellConfig = scriptDir + '/../data/application.conf.hpc'

cromwellCommand = (f'nice java -Dconfig.file={cromwellConfig} '
                   f'-jar /hpc/local/CentOS7/gen/software/wdl-28.0.12/cromwell-28.jar '
                   f'run '
                   f'{wdlPath} '
                   f'{inputJson} ')

subprocess.call(f"sbatch.sh --time=36:0:0 --mem=20G -J {args.sampleName} '"
                + cromwellCommand + "'", bufsize=0, shell = True)

