#!/usr/bin/env perl
## 
## see also the R code in uuutils::read.json which is the producer of these files)
## BTW I'm sure this can be done more easily WDL which has a json object)

die "Usage: 

  merge-compTrim-logs.pl x1.json x2.json ... xN.json > merged-combined.json

" unless @ARGV;

use JSON;
use Data::Dumper;

my $start_regexp="^#+ *[-=]* *json +start";
my $end_regexp="^#+ *[-=]* *json +end";

my $merge={compTrim=>{seen=>0, poly=>0, quality=>0, complexity=>0, survive=>0}};
my @fields=keys %{$merge->{compTrim}};

foreach my $file (@ARGV) {
   my $found=0;
   my $end=0;

  open(FILE, $file) or die "$file: $!";

  my $s=undef;
  while(<FILE>) { 
    if (/$start_regexp/i ) {
      $found++;
      last;
    }
  }
  while(<FILE>) { 
    if ( ! /$end_regexp/i ) {
      $s .= $_;
    } else {
      $end++;
      last;
    }
  }

  close(FILE) or die "$file: $!";
  die "Found 0 or >2 json section in file $file" unless $found ==1;
  die "End of json section in file $file missing" unless $end ==1;
  
  my $j = decode_json($s);
  
  for my $f ( @fields ) {
     my $val=$j->{compTrim}->{$f};
     die "Field 'compTrim' -> '$f' is undefined in file $file\n\n" . Dumper($j) . "\n"
         unless defined($val);
     $merge->{compTrim}->{$f} += $val;
  }
}

print "## -------- JSON START --------\n";
print encode_json($merge), "\n";
print "## -------- JSON END --------\n";
