<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Contents](#markdown-header-contents)
    - [Pipeline Scripts](#markdown-header-pipeline-scripts)
    - [Other scripts](#markdown-header-other-scripts)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Contents

## Pipeline Scripts
 * pipeline.py - driver script that executes all the steps below in order. (now superceded
   by scseq.py)
 * scseq.py - driver script for the Cromwell/wdl pipeline; supercedes pipeline.py 
 * pipeline.wdl - pipeline script to be used with cromwell/wdl; 
 * preprocess_fastq.pl - Extract UMI and cbc information from R1 and integrate it into R2.
 * multiCompTrim.py - Trim low complexity and low quality regions from the 3' end of reads.
 * add_features.pl - Assign each uniquely mapping read to the feature it maps to (if any).
 * tally.pl - Sort reads by well and count the features they are assigned to, producing
   count tables and saturation files
 * platediagnostics.R - (this is a symlink to a script in the platediagnostics R library, see there)

## Other scripts
 * complexityTrimming.py - Trim low complexity and low quality regions
   from the 3' end of reads. Multiple instances of this script are
   executed by multiCompTrim.py in order to increase speed.
 * tools.pm - Set of functions and tools used in various Perl scripts.
