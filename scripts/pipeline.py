#!/usr/bin/env python
import argparse
import os
import tempfile
import shutil
import sys
import subprocess
import re
import json
from collections import namedtuple
from typing import NamedTuple, List
from functools import wraps
from TUtils import HPCJobs, cleanFile, PushDir, ePrint, getVersion, writeDelimitedJSON

#----------------------------------------------------------------------
#note: this is a decorator. decorators can be weird.
#note: they normally take a function and return a modified
#note: version of the function they are applied to.
#note: because this decorator takes an argument, in order for it
#note: to work, it needs to contain another decorator (wich will have access
#note: to the passed variable because it's in the same scope).
#note: also the @wraps decorator  is used in order to preserve introspection capabilities.
#note: this construct is needlessly complicated, but it gets the job done.
def pipelineStep(num: int):
    """set the attribute "step" on the decorated function to be "num"."""
    def setStep(function):
        @wraps(function)  #This needs to be used to preserve introspection
        def wrapper(*args, **kwargs):
            return function(*args, **kwargs)
        setattr(wrapper, 'step', num)

        return wrapper

    return setStep

#----------------------------------------------------------------------
def _parseConfig(configFile: str) -> dict:
    """parse a config file and returns a nested dictionary of its entries"""
    configDict = {}
    cleanedFile = cleanFile(configFile, startsWith = ['#'],
                            skipEmpty = True, strip = True,
                            removeInlineComments = True, )

    for line in cleanedFile:
        if line.startswith("[") and line.endswith("]"):
            header = line.strip("[]")
            configDict[header] = {}
            continue

        key, value = line.split(" = ")
        configDict[header][key] = value

    return configDict

#----------------------------------------------------------------------
def makeConfig(configFile: str) -> NamedTuple:
    """process the config file to nested named tuples"""
    configDict = _parseConfig(configFile)
    configTuple = namedtuple("config", list(configDict.keys()))

    tupleList = []
    for key in configDict:
        subConfig = namedtuple(key, list(configDict[key].keys()))
        tupleList.append(subConfig(*list(configDict[key].values())))

    finalNamedTuple = configTuple(*tupleList)
    return finalNamedTuple

#----------------------------------------------------------------------
def checkBinaries(config: NamedTuple) -> None:
    """load required modules on the HPC environment"""
    for binary in config.binaries:
        if not shutil.which(binary):
            errorMessage = (f'It seems this program cannot be called: {binary} '
                            f'try loading the corresponding module and try again.')
            raise FileNotFoundError(errorMessage)


########################################################################
class Pipeline:
    """perform all the steps required to process single cell
    RNA-seq from fastq to count table"""

    #----------------------------------------------------------------------
    def __init__(self, sampleName: str, config: NamedTuple, inputFiles: List[str],
                 env: dict = os.environ, startingMethod: int = 1, lastMethod: int=0):
        """Constructor"""
        self.sampleName = sampleName
        self.env = env
        self.config = config
        self.startingMethod = startingMethod
        self.lastMethod = lastMethod

        #input file processing
        for index, inputFile in enumerate(inputFiles):
            if not (os.path.isfile(inputFile) or os.path.isdir(inputFile)):
                message = f'could not find (file or dir) {inputFile}.'
                raise IOError(message)
            inputFiles[index] = os.path.abspath(inputFile)

        if startingMethod == 1:
            self.inputFile = inputFiles
        else:
            self.inputFile = ','.join(inputFiles) # (not a real file, maybe should use glob pattern here)

        self.stepsMethodList = self._extractSteps(self)

    #----------------------------------------------------------------------
    @pipelineStep(1)
    def catLanes(self):
        """concatenate files coming from different lanes into one"""
        config = self.config.catLanes
        R1 = [fileName for fileName in self.inputFile if 'R1' in fileName]
        R2 = [fileName for fileName in self.inputFile if 'R2' in fileName]

        if len(R1) != len(R2):
            message = f'the same number of files must contain R1 and R2.'
            raise IOError(message)

        R1input = ' '.join(R1)
        R2input = ' '.join(R2)

        R1output = f'{self.sampleName}-R1.fastq.gz'
        R2output = f'{self.sampleName}-R2.fastq.gz'

        with PushDir(config.folder):
            commandR1 = (f'zcat '
                       f'{R1input} '
                       f'| gzip '
                       f'-n '
                       f'> {R1output}')
            commandR2 = (f'zcat '
                         f'{R2input} '
                         f'| gzip '
                         f'-n '
                         f'> {R2output}')

            R1job = HPCJobs(reqTime = config.reqTime,
                              reqMemory = config.reqMemory,
                              reqCores = config.reqCores,
                              reqTempSpace = config.reqTempSpace,
                              env = self.env)
            R2job = HPCJobs(reqTime = config.reqTime,
                            reqMemory = config.reqMemory,
                            reqCores = config.reqCores,
                            reqTempSpace = config.reqTempSpace,
                            env = self.env)

            R1job.submit(name = 'zcat', command = commandR1)
            R2job.submit(name = 'zcat', command = commandR2)
            R1job.wait()
            R2job.wait()

            self.inputFile = f'{os.path.abspath(R1output)},{os.path.abspath(R2output)}'
    #----------------------------------------------------------------------
    @pipelineStep(2)
    def preprocessFastq(self, ) -> None:
        """process R1 and R2 to extract UMI and cbc tags"""
        outputName = f'{self.sampleName}_cbc.fastq.gz'
        logFile = f'{self.sampleName}-preprocessFastq.log'
        config = self.config.preprocessFastq

        with PushDir(config.folder):
            command = (f"{config.script} "
                       f"--fastq {self.inputFile} " # note: like name_R1.fastq.gz,name_R2.fastq.gz
                       f"--umi_len {config.umiLength} "
                       f"--cbc_len {config.cbcLength} "
                       f"2> {logFile} "
                       f"| gzip -n "
                       f">  {outputName} ")


            self._submitJob(config, "preProc", command)
            self._checkCompletion(logFile, 'preprocessFastq')
            self.inputFile = os.path.abspath(outputName)

    #----------------------------------------------------------------------
    @pipelineStep(3)
    def compTrim(self, ) -> None:
        """perform complexity trimming on a fastq file"""
        outputName = f'{self.sampleName}_cbc_compTrim.fastq.gz'
        logFile = f'{self.sampleName}-compTrim.log'
        config = self.config.compTrim

        with PushDir(config.folder):
            command = (f'{config.script} '
                       f'-l {config.lines} '
                       f'{config.gzip} '
                       f'-s {config.compTrimScript} '
                       f'-x {config.compThreshold} '
                       f'-q {config.qualThreshold} '
                       f'-w {config.window} '
                       f'--length {config.minLength} '
                       f'{self.inputFile} '
                       f'2> {logFile} '
                       f'| gzip -n '
                       f'> {outputName} ')

            self._submitJob(config, "masterTrim", command)
            self._checkCompletion(logFile, 'compTrim')
            self.inputFile = os.path.abspath(outputName)

    #----------------------------------------------------------------------
    @pipelineStep(4)
    def starMapping(self,) -> None:
        """map a fastq file to the genome"""
        outputName = f'{self.sampleName}.bam'
        config = self.config.STARMapping
        logFile = f'{self.sampleName}-starMapping.log'

        def _getcounts(lookfor):
            ## get the vital statistics to the log file:
            ## uniq mappers:
            starlog = 'Log.final.out'      #  fixed
            cmd = f"grep '{lookfor}' {starlog} 2>&1 "
            o = subprocess.check_output(cmd, shell = True).decode()
            o = o.split('|')
            if len(o) != 2:
                raise RuntimeError(f"Could not find '{lookfor} | somenumber' in {starlog}.\n(Found: " . o.join() + "instead\n")
            return int(o[1])

        with PushDir(config.folder):
            command = (f'{config.script} '
                       f'--genomeDir {config.genomeDir} '
                       f'--runThreadN {config.runThreadN} '
                       f'--twopassMode {config.twopassMode} '
                       f'--alignEndsType {config.alignEndsType} '
                       f'--alignSoftClipAtReferenceEnds {config.alignSoftClipAtReferenceEnds} '
                       f'--outSAMunmapped {config.outSAMunmapped} ' # (typical value is 'Within', see config)
                       f'--readFilesCommand {config.readFilesCommand} '
                       f'--outStd {config.outStd} '
                       f'--outSAMtype {config.outSAMtype} '
                       f'--readFilesIn {self.inputFile}'
                       f'> {outputName} '
                       f'2> {logFile}')
            ## when debugging it is faster to not run STAR, but pretend it ran by
            ## copying its output, as follows:
            ## egdir='/hpc/pmc_holstege/tmargaritis/data/Mmkidney/test100k/map.good'
            ## command = f'cp -pR {egdir}/' + '*{log,out}* ./ '

            self._submitJob(config, "STARmap", command)
            self._checkCompletion('Log.out', 'STAR mapping', completionRegexp='ALL DONE.*')

            nuniq = _getcounts('Uniquely mapped reads number')
            nmulti = _getcounts('Number of reads mapped to multiple loci')
            ### now append to logFile.
            with open(logFile, 'a') as file:
                contents = {'mapping' : { 'total':nuniq+nmulti,
                                          'unique':nuniq,
                                          'multi':nmulti }}
                writeDelimitedJSON(contents, file)
            self.inputFile = os.path.abspath(outputName)

    #----------------------------------------------------------------------
    @pipelineStep(5)
    def addFeatures(self,) -> None:
        """determine the feature to which reads have mapped"""
        config = self.config.addFeatures
        logFile = f'{self.sampleName}-addFeatures.log'

        with PushDir(config.folder):
            command = (f'{config.script} '
                       f'--keep '
                       f'--bam {self.inputFile} '
                       f'--prot {config.prot} '
                       f'--clus {config.clus} '
                       f'--anti_lincRNA {config.antiLincRNA} '
                       f'--rest {config.rest} '
                       f'> {self.sampleName}-assign.out '
                       f'2> {logFile}')

            self._submitJob(config, "addFeat", command)
            self._checkCompletion(logFile, 'addFeatures')
            self.inputFile = os.path.abspath(f'{self.sampleName}.bam')

    #----------------------------------------------------------------------
    @pipelineStep(6)
    def processSam(self,) -> None:
        """sort reads to their respective wells"""
        config = self.config.processSam
        logFile = f'{self.sampleName}-processSam.log'

        with PushDir(config.folder):
            command = (f'{config.script} '
                       f'--prefix {self.sampleName} '
                       f'--id2name {config.id2name} '
                       f'--umi_len {config.umiLength} '
                       f'--cbc_len {config.cbcLength} '
                       f'--bar {config.barcodeFile} '
                       f'-allow_mm {config.allowMM} '
                       f'{self.inputFile} '
                       f'> {self.sampleName}.out '
                       f'2> {logFile}')


            self._submitJob(config, "proSAM", command)
            self._checkCompletion(logFile, 'processSam')
            self.inputFile = os.path.abspath(config.folder) # note: must be a folder!


    #----------------------------------------------------------------------
    @pipelineStep(7)
    def plateDiag(self, ) -> None:
        """perform diagnostic of the plate"""
        config = self.config.plateDiag
        logFile = f'{self.sampleName}-plateDiag.log'
        with PushDir(config.folder, ignoreExistance = False):
            command = (f'{config.script} '
                       f'--verbose={config.verbose} '
                       f'--inputdir={self.inputFile} ' # yes, a folder
                       f'--outputdir=. '
                       f'--prefix={self.sampleName} '
                       f'2> {logFile}')

            self._submitJob(config, "plateDia", command)
            self._checkCompletion(logFile, 'plateDia')

    #----------------------------------------------------------------------
    @pipelineStep(8)
    def cleanup(self) -> None:
        """remove all files that are not specified in the config file"""
        config = self.config.cleanup
        savePatternsString = config.save.replace('[sample]', self.sampleName)
        savePatterns = savePatternsString.split(",")

        for pattern in savePatterns:
            for fileToSave in glob.glob(f'**/{pattern}'):
                fileName = os.path.basename(fileToSave)
                os.rename(fileToSave, f'./{fileName}')

        for directory in glob.glob("./*/"):
            shutil.rmtree(directory)



    #----------------------------------------------------------------------
    def _submitJob(self, config: NamedTuple, name: str,
                   command: str, waitCheck: int = 30) -> None:
        """generate an appropriate HPCJobs object, submit
        a job to the HPC and wait for it to finish"""
        ePrint("submitting: ", command)
        job = HPCJobs(reqTime = config.reqTime,
                      reqMemory = config.reqMemory,
                      reqCores = config.reqCores,
                      reqTempSpace = config.reqTempSpace,
                      env = self.env)
        job.submit(name = name, command = command)
        job.wait(waitCheck)

    #----------------------------------------------------------------------
    def pipeline(self):
        """executes the entire pipeline, starting from
        the method specified in startingMethod"""
        for stepMethod in self.stepsMethodList:
            if (stepMethod.step < self.startingMethod or
                (self.lastMethod > 0 and stepMethod.step > self.lastMethod)):
                ePrint(f'skipping step {stepMethod.__name__}, as requested.')
                continue
            ePrint(f'executing {stepMethod.__name__}')
            stepMethod()
            ePrint('finished executing... moving on to the next step.')
    #----------------------------------------------------------------------
    @classmethod
    def stepString(cls, ):
        """generate a string that enumerates the steps of the pipeline
        in order with their respective docstrings"""
        outputString = ''
        stepList = cls._extractSteps(cls)

        for step in stepList:
            outputString = outputString + f'{step.step}\t{step.__doc__}\n'

        return outputString

    #----------------------------------------------------------------------
    @classmethod
    def _extractSteps(cls, obj):
        """extracts and orders methods that have the "step" attribute from
        either a class or instance."""
        unorderedStepList = []

        #out of all class attributes, save the ones that have a step attribute
        for attributeName in dir(obj):
            attribute = getattr(obj, attributeName)
            if not hasattr(attribute, 'step'):
                continue
            unorderedStepList.append(attribute)

        stepList = sorted(unorderedStepList, key = lambda x: x.step)

        return stepList

    #----------------------------------------------------------------------
    def _checkCompletion(self, logFile, functionName,
                         completionRegexp='successfully completed', # what to look for
                         lastNlines=8):                            # where
        """check that the program successfully completed"""
        pat = re.compile(completionRegexp, flags=re.IGNORECASE)
        with open(logFile, 'r') as log:
            for line in log.readlines()[-lastNlines:]:
                if pat.search(line):
                    return True
            message = (f'it seems there was a problem with {functionName}, '
                       f"I could not find the 'success' marker.\nSee {logFile} and other log files for details")
            raise IOError(message)

if __name__ == '__main__':

    methodExplaination = Pipeline.stepString()

    #help messages
    description = (f'Perform the entirety of the sc-SEQ pipeline, '
                   f'starting from an arbitrary step.')
    configHelp = (f'Path to the config file containing arguments '
                  f'for all the scripts that will be called, and more.')
    startingHelp = (f'An integer representing the step number that should be '
                    f'executed first, all subsequent steps will also be executed.'
                    f' the input(s) should match that step input requirements. '
                    f'here is an overview of the current pipeline steps and what'
                    f'they do:\n {methodExplaination}')
    lastHelp = (f'The last step number that should be executed. 0 means: all steps')
    nameHelp = (f'The name of the sample being processed.')
    inputHelp = (f'File(s) that will be passed as input to the first step of the pipeline')

    #options
    options = argparse.ArgumentParser(description = description,
                                      formatter_class = argparse.ArgumentDefaultsHelpFormatter)
    options.add_argument("--configFile",
                         help = configHelp,
                         default = '/hpc/pmc_holstege/scseq/data/humanConfig.txt', )
    options.add_argument('sampleName',
                         help = nameHelp,
                         type = str)
    options.add_argument("inputFile",
                         help = inputHelp,
                         nargs = '+')
    options.add_argument('-s', '--startingStep',
                         help = startingHelp,
                         type = int,
                         default = 1)
    options.add_argument('-l', '--lastStep',
                         help = lastHelp,
                         type = int,
                         default = 0)

    args = options.parse_args()

    #action

    #checking and logging version
    version = getVersion(__file__)
    ePrint(f'executing {__file__}, version {version}')

    #check presence of binaries, loads config data and set environment
    config = makeConfig(args.configFile)
    checkBinaries(config)

    #initializes the Pipeline Object
    P = Pipeline(sampleName = args.sampleName,
                 config = config,
                 inputFiles = args.inputFile,
                 startingMethod = args.startingStep,
                 lastMethod = args.lastStep)

    P.pipeline()
