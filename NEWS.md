# version v90 (4-May-2022)

    * sortseq, not SORTseq

# version v89 (4-May-2021)

    * inferring umilength automatically from barcode file to avoid inconsistencies

# version v88 (29-Apr-2021)

    * also works with yeast genome; added Queens' protocol barcodes; misc. small changes

# version v87 (26-Feb-2021)
    
    *  *.platediagnostics.log and *.perwellfits.txt now also kept as output from the
    plateiagnostics stage

# version v86 (26-Feb-2021)

    * now works with slurm (not with SGE anymore). Also uncovered and fixed a weird 
    featureCounts filename length limitation.

# version v85 (2021-01-15)

	* moved SCutils and platediagnostics to their own repo for easier
	installing & maintenance
	* version bump to v85

# version v84 (2021-01-06)

	* ignored wells now also ignored in the accumulation plots

# version v83 (2020-12-10)

	* various ... e.g. at least 500; proper rel scale for saturation derivative; better
	code; no more markings in plate.plot; reporting various saturation characteristics, also
	per well. no more distinction between empty/full wells for ERCC plot.

# version v69 (2020-04-08)

	* proper scaling of the color scale under plate.plot

# version v68 (2020-04-02)

	* added --min_liveness_cutoff argument (preliminary) and made thing robust against too few
	emptywells

	* added --maxLength param to allow limiting read2 length so as to mimick shorter sequencing

# version v66 (2020-03-23)

	* extra options to plate.plot for more general use; many extra checks

# version v65 (2020-02-19)

	* mostly small changes, but using much larger runtime requirements (HPC problems) and
	      using pmc_holstege instead of pmc_gen

# version v64 (2019-10-15)

	* minor (editorial) changes

# version v63 (2019-10-14)

	* now keeping reads that are (or have become) too short as a result of trimming to
	      SAMPLENAME-compTrim-tooshort.fastq.gz (with ':thereason' appended to line1).
	      multiCompTrim was therefore replaced with WDL scatter, + concomittant changes.

# version v62 (2019-09-12)

	* added new empties specifications; made both --species and --empties arguments
	mandatory

# version v61 (2019-08-30)

	* changed arguments to pre_process_fastq.pl (also in WDL script and default json files) to
	be more flexible specifying where CBC and/or UMI are in read1 (can also be 'none').

# version v60 (2019-08-01)

	* including Log.final.out, output differnt location, more extensive help

# version (2019-06-21)

	* some restructuring (non-public stuff went to other repo).
	column1 and col1 as shorthands.

# version v55 (2018-11-22)

	* Sharq.py now simply fills out json template and submits cromwell
	job to the queue directly. Version

# version v54 (2018-10-10)

	* *.elog files now *.log

# version v53 (2018-09-03)

	* Degraded mode now using top3 ERCC's if there are  too few
	ERCCs. Displayed conspicuously.

# version v52 (2018-08-29)

	* marking ignored wells; logarithmic emptywell barplot

# version v51 (2018-08-24)

	* marked empty wells

# version v50 (2018-08-24)

	* allowing wells to be ignored completely

# version v49 (2018-08-22)

	* allowing different sets of empty wells

# version v48 (2018-08-09)

	* cumulative plot now shows data from all
	good non-empty wells, and also shows liveness cutoff

# version v47 (2018-06-25)

	* goodERCCs now outside goodwells. Added testsuite

# version v46 (2018-05-07)

	* started maintaining ChangeLog :-)

# version v45 (2018-04-26)

	* added 'degraded mode' in order to call live cells even if not enough ERCC's can be called

# version ? (2018-19-01)

	* version as deposited in BioArXiv
