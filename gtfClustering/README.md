<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [gtfClustering](#markdown-header-gtfclustering)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# gtfClustering

The R script can be used to produce a GTF file in which genes with overlapping exons are clustered into single genes. This helps prevent discarding of reads that otherwise would  be multimappers. The code and comments in `cluster-GTFprotein_coding.R` should be enough to make things clear. If all is well the script can be run in stand-alone fashion, but running it interactively may be easier.

For more details and justification see 
[Candelli et al., bioRxiv 2018](https://doi.org/10.1101/250811).
 
