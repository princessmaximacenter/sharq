#!/usr/bin/env Rscript

usage <- function()cat("
Script that merges overlapping protein_coding genes from an GENCODE GTF
file, and produces a new file with the overlapping genes merged. The aim
is to avoid reads lying in the overlap region being ignored by
featureCounts (or htseq.count) as it won't know what gene to assign the
read to. Clusters are named after the member having the most GO
annotations (or the lowest ENSG-number when unavailable or tied), using
both gene_id and gene_name (e.g. ENSG00000243709__LEFTY1-cluster)
The rationale is that these are probably the most well known and stable
representants of the cluster. All the transcripts of a cluster's genes,
and all the exons of these transcripts get the same gene_id. The
original gene_id and _name are available as orig_gene_id and
orig_gene_name.  Clusters are sorted by genomic coordinate (the
ancillary -id2name.txt and -overlapping.txt files are not). 

Usage: cluster-GTFprotein_coding.R --gtf gencode-vX.Y.gtf \\
    --GOassoc Hs_EnsGenes89_GOassoc-30jun2017.txt \\
    --outprefix clusters-vX.Y

Output:
   outprefix-id2name.txt              mapping from gene_id to gene_name (all features)
   outprefix-overlapping.txt          all pairs of overlapping protein coding genes
   outprefix-clus.txt 		      cluster members using gene_id (ENSG ids)
   outprefix-clus-byname.txt          cluster members using gene_name (HGCN names)
   outprefix-clusters.gtf             merged genes, with source 'PMCgenecluster'.

The GO annotations must be downloaded manually from EnsMART since AnnotationDbi
uses Entrez genes which do not map uniquely to EnsEMBL. 
Go to http://www.ensembl.org/biomart/martview/ and do

  Use: Human genes GRCh38.p10,
  Filter: none
  Attributes:
     Features
     Gene: only Gene Stable ID; 
     External:
       GO: GO term accession and GO term evidence code

The end result should be a table with headers
Gene_stable_ID \\t GO_TERM_accession \\t GO term evidence code
followed by the data. 

Written by <plijnzaad@gmail.com>
")

library(GenomicRanges)
library(uuutils)                        #for e.g. getversion
library(parseArgs)
library(rtracklayer)
library(igraph)                         # clusters are found as connected components of graph

if(interactive()) { 

    cat("Clearing complete workspace")
    ## keep <- ls(pat="???.*")
    ## rm(list= setdiff(ls(),keep)) #to avoid copypaste-errors in interacive mode

    ## dir <- "/hpc/local/CentOS7/gen/data/genomes/mouse/gencode/"
##    dir <- "/Users/philip/proj/gencode/human/"
    dir <- "/Users/philip/proj/gencode/mouse/"
    setwd(dir)
##     actuals <- c(paste0("--gtf=",dir,"gencode.v28.annotation.gtf.gz"),
##                  "--outprefix=clusters",
##                  paste0("--GOassoc=",dir,"GRCh38.p12-goASSOC5jul2018.txt.gz"))
    actuals <- c(paste0("--gtf=",dir,"gencode.vM17.annotation.gtf.gz"),
                 "--outprefix=clusters",
                 paste0("--GOassoc=",dir,"GRCm38.p6-goASSOC5jul2018.txt.gz"))
    setwd(dir)

    rdafile <- "gencode.v29.annotation.rda"
    if(FALSE)
      load(rdafile)

    script <- "cluster-GTFprotein_coding.R?"
    script.version <- "(version unknown)"

} else {
    actuals <- NULL                       #will come from cmd line
    args <- commandArgs(trailingOnly = FALSE)  
    path <- normalizePath(dirname(sub("^--file=", "", args[grep("^--file=", args)])))
    support.functions <- paste(path, "plate_diagnostics_functions.R", sep="/")
    script <- paste(path, "cluster-GTFprotein_coding.R", sep="/")
    script.version <- getversion(script)
}

args <- parseArgs(.overview=usage,
                  gtf=NA,
                  GOassoc=NA,
                  outprefix=NA,
                  .actuals=actuals)
arglist <- paste0('--',names(args), "=", unlist(args), collapse=" ")
header <- paste(script, script.version, 'with arguments', arglist, collapse=" ")

gtf <- import.gff(args$gtf)
if(FALSE)
  save(gtf, file=rdafile)

## first get rid of pseudo autosomals that only yield meaningless multimappers:
## par_y <- grepl("ENSG\\d{11}\\.\\d+_PAR_Y", gtf$gene_id, perl=TRUE)
par_y <- grepl("ENS(MUS)?G\\d{11}\\.\\d+_PAR_Y", gtf$gene_id, perl=TRUE)
warning("ignoring ", sum(par_y), " features on the Y pseudo-autosomal region")
gtf <- gtf[!par_y]

all.genes <- gtf[gtf$type == 'gene']
ensid <- sub("\\..*", "", all.genes$gene_id)
all.genes$ensid <- ensid
names(all.genes) <- ensid

file <- "id2name.txt"
warning("writing ", file)
write.table(file=file,
            values(all.genes)[,c("ensid", "gene_name")],
            quote=FALSE, row.names=FALSE, sep="\t", col.names=FALSE)

exons <- gtf[gtf$type == 'exon']
ensid <- sub("\\..*", "", exons$gene_id)
exons$ensid <- ensid
rm(ensid)

PCgenes <- all.genes[all.genes$gene_type == 'protein_coding'] # Hs:19799; Mm: 21948

PCtxpts <- gtf[ gtf$type == 'transcript' & gtf$gene_type == 'protein_coding'] # Hs: 146213; Mm: 93308

all.PCexons <- exons[exons$gene_type == 'protein_coding'] ## Hs:1062097; Mm: 697946 # with duplicates!

### find overlaps on exon level, but be sure to get rid of the duplicates:

exon.ids <- unique(all.PCexons$exon_id) # Hs: 569764 ; Mm: 423729
dups <- duplicated(all.PCexons$exon_id)
PCexons <- all.PCexons[ !dups ]

o <- findOverlaps(PCexons, PCexons)

## exclude self-self and reverse hits, but also hits within the same gene:
PCexons.pairs <- o[   queryHits(o) < subjectHits(o)
                   &  PCexons[queryHits(o)]$ensid != PCexons[subjectHits(o)]$ensid, ]
rm(o)

exon.overlap.idx <- unique(c(queryHits(PCexons.pairs),subjectHits(PCexons.pairs)))

overlapping.exons <- PCexons[exon.overlap.idx] # Hs: 24077; Mm: 7461
warning("Genes involved in overlaps: ", 
        length(unique(overlapping.exons$ensid))) ## => Hs: 2157 different ensids; Mm: 978

q <- queryHits(PCexons.pairs)
s <- subjectHits(PCexons.pairs)

overlapping.gene.ids <- unique(overlapping.exons$ensid)
overlapping.genes <- PCgenes[overlapping.gene.ids]

o <- findOverlaps(overlapping.genes, overlapping.genes) # Hs: 5737 hits; Mm: 3066
q <- queryHits(o)
s <- subjectHits(o)
                                        # get rid of self-self and reverse overlaps:
gene.pairs <- o[ q < s &  overlapping.genes[q]$ensid != overlapping.genes[s]$ensid, ]
warning("Total number of overlaps: ", 
        length(gene.pairs))  ##  => Hs:1790 pairs (involving 2157 genes); Mm: 1044 (978)

### find their names:

q <- queryHits(gene.pairs) 
s <- subjectHits(gene.pairs)
u <- unique(c(q,s))
## length(u)                     # 2157, double check

q.genes <- overlapping.genes[q]
s.genes <- overlapping.genes[s]

d <- data.frame(q.id=q.genes$ensid, q.name=q.genes$gene_name, q.length=width(q.genes),
                s.id=s.genes$ensid, s.name=s.genes$gene_name, s.length=width(s.genes))

file <- paste0(args$outprefix, "-overlapping.txt")
warning("Creating ", file)
cat(file=file, "## ")
write.table(file=file, append=TRUE, d, row.names=FALSE, quote=FALSE, sep="\t")

## find the clusters as connected components of a graph

q <- queryHits(gene.pairs)
s <- subjectHits(gene.pairs)

## NOTE: igraph insists on vertices being called 1:N, therefore
## temporily translate the indexes into gene.pairs.

## set up vertex names starting from 1: 
u <- sample(unique(c(q,s)))                #randomize so we don't get confused
vertex2idx <- u
names(vertex2idx) <- 1:length(u)
idx2vertex <- 1:length(u)
names(idx2vertex) <- u

g <- graph.empty()
g <- add.vertices(g, nv=length(u))

## Be aware that we use a named vector as a hash, so be sure to cast to
## character when using the index
edges <- matrix(unname(c(idx2vertex[as.character(q)],
                         idx2vertex[as.character(s)])),
                nrow=2, byrow=TRUE)

g <- add.edges(g, edges=as.vector(edges))

warning("Found ", no.clusters(g), " clusters") # Hs: 792 connected components; Mm: 368

clusters.byindex <- groups(components(g))
clus.sizes <- sort(unname(unlist(lapply(clusters.byindex, length))))
s <- summary(clus.sizes)
warning("Summary of cluster sizes:\n",
        paste(names(s), collapse="\t"),
        "\n",
        paste(unname(s), collapse="\t"),
        "\n")

## translate back to index into overlapping.genes, 
clusters.byindex <-
  lapply(clusters.byindex, function(verts){unname(vertex2idx[verts])})

clus.name.format <- function(gene)sprintf("%s__%s", gene$ensid, gene$gene_name)

## give decent names to members
clusters <-
  lapply(clusters.byindex,
         function(idx)clus.name.format(overlapping.genes[ idx ]))

go <- read.delim(file=args$GOassoc, header=TRUE)
## format: tab delimited, with columns GO.term.evidence.code,
##  Gene.stable.ID, GO.term.accession. See usage message on how to
## download from ensembl biomart.

use <- go$GO.term.evidence.code != "IEA" # ignore the '
warning("Read ", nrow(go), " GO associations, will discard ", sum(!use), " that are Inferred by Electronic Annotation")
go <- go[use,c( "Gene.stable.ID", "GO.term.accession")]
names(go) <- c("ensid", "goid")
go.per.ensid <- unstack(go, goid ~ ensid)
nterms.per.ensid <- unlist(lapply(go.per.ensid, function(terms)sum(terms!="")))

choose.clus.name <- function(clus) {
    ## (clus is integer index into overlapping.genes!)
    ## Invent a somewhat human-usable name for the clusters: use the
    ## name that has most GO terms, or if unavailable or tied, use the
    ## lowest-sorting ENSG-id
    gene.ids <- overlapping.genes[clus]$ensid
    terms.per.id <- nterms.per.ensid[ gene.ids ]
    names(terms.per.id) <- gene.ids     #in case  they are <NA>
    na <- is.na(terms.per.id)
    terms.per.id[ na ] <- 0
    pick <- order(-terms.per.id, gene.ids)[1]
    clus.name.format(  (overlapping.genes[clus])[pick] )
    ## sprintf("%s-cluster", nameclus[pick])
}                                       # choose.clus.name

clusnames <- unname(unlist(lapply(clusters.byindex, choose.clus.name)))

names(clusters) <- paste0(clusnames, '-cluster')

## sort them by chr and coord
list <- lapply(clusters.byindex, function(clus){ overlapping.genes[ clus ][1]} )
gr <- unlist(GRangesList(list))
clusters <- clusters[ order(gr) ] 

print.clusters <- function(file, clusters, header) {
    ## lines <- unlist(lapply(list, function(elt)paste(elt, collapse="\t")))
    names <- names(clusters)
    members <- unlist(lapply(unname(clusters),
                             function(clus)paste(sort(clus), collapse=", ")))
    warning("Creating file ", file)
    cat(file=file, header)
    cat(file=file, append=TRUE, paste(names, members, sep="\t", collapse="\n"))
}

h <- paste0("## ", header, "\n","## cluster_id\tcluster_members\n")

print.clusters(clusters=clusters,
               file=paste0(args$outprefix,"-clus.txt"), header=h)

### create a GTF file of merged genes: all txpts of the original genes
### now become transcripts of one gene_cluster, and the gene_cluster
### becomes the new gene.

output.gtf <- paste0(args$outprefix, "-clusters.gtf")
if (file.exists(output.gtf))  { 
    warning("Seeing file ", output.gtf , ", will overwrite it\n")
    unlink(output.gtf)
}
warning("Creating file ", output.gtf)

cat(file=output.gtf, sep="",
    "##contact: ", Sys.getenv("USER"),"\n",
    "##date: ", as.character(Sys.time()), "\n",
    "##description: merged overlapping protein_coding genes generated by ", header, "\n",
    "##format: gtf\n",
    "##provider: Princess Maxima Center for Pediatric Oncology\n")

source.name <- 'PMCgenecluster'
for (clusname in names(clusters)) {
    members <- clusters[[clusname]]
    ensids <- gsub("__.*$","",members)
    genes <- PCgenes[ensids]

    super.gene <- GRanges(range(genes), source=source.name,
                          type='gene',score=NA, phase=NA,
                          gene_id=clusname, gene_name=clusname,
                          gene_type="protein_coding")

    export.gff(con=output.gtf, append=TRUE, format='gtf', object=super.gene)

    g.ids <- genes$gene_id # includes the gene version
    txpts <- PCtxpts[ PCtxpts$gene_id %in% g.ids ]
    names(txpts) <- txpts$transcript_id

    for (t.id in txpts$transcript_id) {
        ts <- txpts[t.id]
        names(ts) <- NULL
        ts$source <- source.name
        ts$orig_gene_id <- ts$gene_id
        ts$orig_gene_name <- ts$gene_name
        ts$gene_id <- clusname
        ts$gene_name <- clusname
        export.gff(con=output.gtf, append=TRUE, format='gtf', object=ts)
        exons <- all.PCexons[ all.PCexons$transcript_id == t.id ]
        exons$source <- source.name
        exons$orig_gene_id <- exons$gene_id
        exons$orig_gene_name <- exons$gene_name
        exons$gene_id <- clusname
        exons$gene_name <- clusname
        exons$ensid <- NULL
        export.gff(con=output.gtf, append=TRUE, format='gtf', object=exons)
    }
}                                       # clus

sessionInfo()

