#!/usr/bin/perl -w
# Usage: $0 --barcodefile barcodes.csv [ -allow_mm=1 ]
# Written by <plijnzaad@gmail.com>
use strict;

use File::Basename;
use FileHandle;
use Getopt::Long;
use tools;
use mismatch;

my $version=getversion($0);
warn "Running $0, version $version\nwith arguments:\n  @ARGV\n";

our ($barcodefile, $umi_len, $cbc_len, $allow_mm, $emptywells, $help, @argv_copy);

@argv_copy=@ARGV;


die "script is not tested yet ...";

my $usage = "

Adds readgroups to reads, based on cell barcodes.  Uses A1 .. P24 as readgroups. Also adds the 
(mismatch-corrected) barcode as a 'BC:Z:CCAGTTAC' tag (as per SAM format), and the following private tags:
zu:Z:XXXXXX The UMI, given by XXXXXX
zm:i:N  The number of mismatches N of the cell barcode, with 9 meaning: 'too
        many' i.e. it could not be rescued. (Only if --allow_mm was specified)
zf:i:N  The well status, with N == 0 for empty and 1 for non-empty. (Only if --emptywells was specified)

Usage: samtools view -h in.bam \\
       | $0 --barcodefile barcodes.txt [ --emptywells=FILEorSTRING]  [ -allow_mm=1 ]  \\
       | samtools view - -hb > out.bam

Arguments: 

--barcodefile FILE  File with cell bar codes (format: id \\t sequence)

Options:

--allow_mm N        How many mismatches to allow in the cell barcodes (default: 0)

--emptywells FILEorSTRING If an existing file: file with names of empty wells
                    (comma and/or line separated) If not an existing file
                    and it's a comma-separated string, those wells will be
                    used. Lastly, the predefined string 'col24' will be
                    used, translating to A24,B24, .. P24

--help              This message
";

die $usage unless GetOptions('barcodefile=s'=> \$barcodefile,
                             'emptywells=s'=> \$emptywells,
                             'allow_mm=i'=> \$allow_mm,
                             'help|h' => \$help);

die $usage if $help;

die "--barcodefile FILE missing or not found\n\n$usage\n" unless $barcodefile;

my $barcodes_mixedcase = mismatch::readbarcodes($barcodefile); ## eg. $h->{'AGCGtT') => 'M3'
my $barcodes = mismatch::mixedcase2upper($barcodes_mixedcase);     ## e.g. $barcodes->{'AGCGTT') => 'M3'
my $well2bc=mismatch::invert_hash($barcodes);                      # e.g. $well2bc->{M3) => 'AGCGTT'

my $mismatch_REs=undef;

if ($allow_mm) { 
  $mismatch_REs = mismatch::convert2mismatchREs(barcodes=>$barcodes_mixedcase, allowed_mismatches =>$allow_mm);
}
$barcodes_mixedcase=undef;              # not used in remainder, delete to avoid confusion

my @cbcs = sort  (keys %$barcodes);     # e.g. 'AGCGTT'
my @wells = sort { mismatch::byletterandnumber($a,$b) }  (values %$barcodes);

if ($emptywells) { 
  if (-f $emptywells) {
    $emptywells=read_emptywells($emptywells, \@wells);
  } elsif ($emptywells eq 'col24' ) { 
    $emptywells = [qw( A24 B24 C24 D24 E24 F24 G24 H24 I24 J24 K24 L24 M24 N24 O24 P24 )];
  } elsif ($emptywells =~ /,/ ) { 
    $emptywells=[split(',', $emptywells)];
  }
}
map { die "unknown emptywell: $_ " unless defined $well2bc->{$_} } @$emptywells;
my $h={};
map { $h->{$_}++ } @$emptywells;
$emptywells=$h;

## mismatched cell barcodes:
my $nreads=0;
my $nmm=0;
my $nrescued=0;
my $nfailedrescue=0;
my $printed_readgroups=0;

LINE:
while(<>) { 
  if (/^@/) { print; next LINE;}
  if (!$printed_readgroups++) { 
    mismatch::print_barcode_readgroups($barcodes, undef);
    print "\@PG\tID:add_readgroups.pl\tCL:".join(' ', ($0, @argv_copy))."\n";
    next LINE;
  }

  chomp $_;
  $nreads++;

  my @read = split("\t",$_);
  my ($QNAME,$FLAG,$RNAME,$POS,$MAPQ,$CIGAR,$MRNM,$MPOS,$ISIZE,$SEQ,$QUAL,@rest)=@read;

  my ($cbc,$umi);
  my(@parts)=split(':', $QNAME);
  for my $tag ( @parts ) { 
    $cbc= $1 if $tag =~ /cbc=([A-Z]+)/i;
    $umi= $1 if $tag =~ /umi=([A-Z]+)/i;
  }
  my $cbcorig=$cbc;
  die "$0: could not find cbc= or umi= in id $QNAME of stdin " unless $cbc && $umi;

  my $mm=0;
  if (! exists $barcodes->{$cbc}) {  
    $mm=1;
    $nmm++;
    if ($allow_mm) {
      $cbc=mismatch::rescue($cbc, $mismatch_REs);      # gives back the barcode without mismatches (if it can be found)
      my $good=defined($cbc);
      $nrescued += $good;
      $nfailedrescue += !$good;
    } 
  }
  my $well;
  if ($cbc) { 
    $well= $barcodes->{$cbc}
  } else { 
    $well='UNK';
    $cbc=$cbcorig;
    $mm=9;
  }
  my $emptystatus="";
  if ($emptywells) {
    if ( $well ne 'UNK') { 
       $emptystatus= defined($emptywells->{$well})? "zf:i:0" : "zf:i:1";
    }
  }
  push @read, join("\t", ("RG:Z:$well", "BC:Z:$cbc", "zm:i:$mm", "zu:Z:$umi"));
  push @read, $emptystatus if $emptystatus;
  print join("\t", @read) . "\n";
}                                       # LINE

warn "Found $nreads reads, $nmm mismatching barcodes, rescued $nrescued, failed to rescue $nfailedrescue of them\n";

sub read_emptywells { 
  my($emptywells)=@_;
  open(FILE, $emptywells) or die "$emptywells: $!";
  $emptywells=[];
  while(<FILE>) { 
    s/[\r\n]*$//;
    push(@$emptywells, split(/,/, $_));
  }
  close(FILE);
  $emptywells;
}
