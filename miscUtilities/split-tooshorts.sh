#!/bin/sh -x
# -*- mode: sh; -*-
#
# The complexityTrimming.py script can dump the reads that become too
# short to one fastq.gz file (when asked politely, using the --skipOut
# option) .  Their reads names (lines 1 + 4n) are appended with the
# reasons they were trimmed, namely ':badpoly', ':badqual' and
# ':badcmplxty'. This script splits this file in to three separate
# files, one for each of the reasons. The output files are named
# by the reason. Their basename is everything up to the first -, _ or .
# Note: it's not the fastest script since it reads through the input file several
# times

if [ $# -eq 0 ] ; then
    echo  "Usage: split-tooshorts.sh file.fastq.gz" >&2
    exit 4
fi

types="badpoly badqual badcmplxty"

file="$1"
basename=$(echo "$file" | sed 's/[._-].*//')
echo $basename

for type in $types; do
    out="$basename-$type.fastq.gz"
    nice zcat "$file" \
        | paste - - - - \
        | grep ":$type" \
        | tr '\t' '\n' \
        | gzip -n > $out
done

