#!/bin/sh -x

cd /home/gen/philip/hpc/Sharq2

dir=/hpc/pmc_holstege/sc-facility/sortseq/rawdata

root=PMC-TM-257_HCMYMBGXC_S4 # _L001_R1_001.fastq.gz

lines=10000

for lane in 1 2 3 4; do
  for read in 1 2; do 
      zcat $dir/${root}_L00${lane}_R${read}*.fastq.gz \
      | head -$lines | gzip -n > test-L$lane-R$read.fast.gz
  done
done
