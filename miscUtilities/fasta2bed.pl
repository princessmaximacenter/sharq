#!/usr/bin/env  perl
## create a simplistic BED file for a Fasta file representing extraneous
## transcripts such as ERCC's and GFP-tagged constructs.  It is needed
## when e.g. creating an IGV genome
use strict;

use Getopt::Long;
use Bio::SeqIO;

my $usage = "fasta2bed.pl [ source ] <  sequences.fa   > sequences.bed";

my $in  = Bio::SeqIO->newFh(-fh => \*STDIN , '-format' => 'fasta');

SEQ:
while ( 1  ) { 
    eval { 
        $_=<$in>;
    };

    last unless $_;
    my $id=$_->primary_id();
    my $len=$_->length();
    print join("\t", ($id, 0, $len-1,  $id)) . "\n";
}                                       # SEQ
