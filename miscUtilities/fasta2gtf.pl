#!/usr/bin/env  perl
## create a simplistic GTF file for a Fasta file representing extraneous
## transcripts such as ERCC's and GFP-tagged constructs.  It is needed
## when assigning reads to such features using e.g. STAR + featureCount
use strict;

use Getopt::Long;
use Bio::SeqIO;

my $usage = "fasta2gtf.pl [ source ] <  sequences.fa   > sequences.gtf";

my $in  = Bio::SeqIO->newFh(-fh => \*STDIN , '-format' => 'fasta');

## not printing the '## description ...' line as it
## usually it will be appended onto the realgenome.gtf file

my $source='extra';
$source = shift(@ARGV) if @ARGV>0;
die "$usage" if @ARGV;

SEQ:
while ( 1  ) { 
    eval { 
        $_=<$in>;
    };

    last unless $_;
    my $id=$_->primary_id();
    my $len=$_->length();
    foreach my $ft ( qw(gene transcript exon) ) { 
      my $annot = qq[gene_id "$id"; ];
      $annot .= qq[transcript_id "tx$id"; ] if $ft eq 'transcript';
      $annot .= qq[transcript_id "tx$id"; exon_number 1; ] if $ft eq 'exon';
      print join("\t", ($id, $source, $ft, 1, $len, '.', '+', '.', $annot)) ."\n"; 
    }
}                                       # SEQ
