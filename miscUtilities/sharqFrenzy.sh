#!/bin/sh
# -*- mode: sh; -*-
##
## Example code for launching several Sharq instances simultaneously.
##
## The *.fastq.gz files  are expected to be laid out
## like species/samplename/*.fast.gz
## The datasheet.txt contains two columns, samplename  and species
## The out-commented code below organizes this as symlinks.
## The un-commented code submits the different cromwell instances.
## submitting this script itself takes a lot of time, so it should
## be done like
##
##     nohup nice ./bulkstart.sh  > bulkstart.log 2>&1 & 
##

###### code to organize the files:
## cd /hpc/pmc_holstege/tmargaritis/data/transfer24April/.
## cat datasheet.txt | while read sample species; do
##     pushd $species
##     mkdir $sample
##     pushd $sample
##     for i in $(find ../.. -name "$sample*fastq.gz"); do
##         ln -s $i ./
##     done
##     popd
##     popd
## done
## 
## cat datasheet.txt | while read sample species; do
##     pushd $species/$sample
##     ls -ld *fastq.gz
##     rename 's/_.*L00/_L/;s/_001//;' *.fastq.gz
##     popd
##     echo '========'
## done

module load Sharq

cd /hpc/pmc_holstege/tmargaritis/data/transfer24April/.

set -x

for species in human mouse; do 
    cat datasheet.txt | grep  $species | while read sample species; do
        pushd $species/$sample
        nohup nice Sharq.py --species $species $sample > $sample-cromwell.out 2> $sample-cromwell.log  & 
        sleep 120 # needed so as not to overload the submit host!
        popd
        echo '========'
    done
done 2>&1 |  tee bulkstart.log  & 

echo "To kill things do qdel -u philip and check your python and java processes"
