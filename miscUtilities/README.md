<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [miscUtilities](#markdown-header-miscutilities)
    - [misc scripts](#markdown-header-misc-scripts)
    - [example files](#markdown-header-example-files)
    - [documentation](#markdown-header-documentation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# miscUtilities

This directory contains assorted scripts and examples to help run Sharq.

## misc scripts
 * mapperOptimizer.py - script for doing parameter sweeps (originally for optimizing mapping parameters)
 * count.sh - used to postprocess mapperOptimizer output
 * pre-commit - example pre-commit script to help you avoid commiting syntactically wrong code to git. Instructiosn included
 * fasta2gtf.pl - can come in handy when creating genomes + annotation, specifically for ERCCs and artificial constructs
 * sharqFrenzy.sh - example script to launch several Sharq instances simultaneously (this should be done carefully since starting Cromwell is very demanding on the submit host)
 
## example files

 * sharq.lua.example - if you're using the `lmod` system, this is an example for a module file
 * env.sh.example - a bit like sharq.lua.example
 * args_star.txt.example - example input for the mapperOptimizer

## documentation 
 * README.md - this file
 * creating-genome+annotation.txt - notes on how to create the genome annotation
 * add_readgroups.pl - add well-names as readgroups to a sam/bam file (untested)
