#!/bin/sh -x
# -*- mode: sh; -*-
#
# script to rerun plate diagnostics that were already run.
# Only requirement is that it it is in the directory where
# all the files needed to run it are present, typically
# the output directory of a platediagnostics run.
# It creates a directory 'plate-rerun' in that location where
# the new output files are created, i.e.: 
#
# *.filesfile.txt
# *.platediagnostics.stats.txt
# *-rerun.out
# *.platediagnostics.live.txt
# *.platediagnostics.pdf
# *-rerun.log
# *.platediagnostics.warnings.txt
# 
# To test the current script, make sure that platediagnostics.R is
# on your $PATH, and then
# $ cd $gitrepo/Sharq/platediagnostics/test/testdata
# $ ~/git/Sharq/miscUtilities/plate-rerun.sh -nobatch test [ further arguments like --ignore=OPodd ]

module load Sharq

set -eux

if [ $# = 0 ]; then
    echo "Usage: $0 [ -nobatch ] plateid"
    exit 1
fi

if [ "$1" = '-nobatch' ]; then
#    cmd=echo # when debugging
    cmd="sh -c "
    shift
else
    ## cmd="qrun.sh -n -N platererun "  ### when testing
    cmd="qrun.sh -N platererun "
fi

plate="$1"
shift

outdir=plate-rerun

if [ -d $outdir ]; then
    echo "Directory $outdir already exists, will overwrite stuff" >&2
else
    echo "Creating directory $outdir already exists" >&2
    mkdir -p  $outdir
fi


filetypes=$(cat <<ZZZZZZZZ
rawcounts.txt
saturation.txt
transcripts.txt
umicounts.txt
wellsat.genes.txt
wellsat.umis.txt
compTrim.log
STAR.log
addFeatures.log
processSAM.log
ZZZZZZZZ
)


filesfile=$outdir/$plate.filesfile.txt
cp /dev/null $filesfile

echo $filetypes

for filetype in $filetypes; do 
    file=$plate.$filetype
    if \ls $file >> $filesfile ; then
        :
    else
        echo "File $file missing " 1>&2
        rm $filesfile
        rm -fr $outdir
        break
    fi
done

if [ -f $filesfile ]; then
    $cmd "platediagnostics.R \
      --verbose \
      --pdf=$plate.platediagnostics.pdf \
      --filesFrom=$filesfile \
      --outputDir=$outdir $* > $outdir/$plate-rerun.log 2> $outdir/$plate-rerun.out "
else
    echo "Files not complete for $plate"  1>&2 
fi

exit
