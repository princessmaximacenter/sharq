To install Sharq, best look at `miscUtilities/sharq.lua.example` in this repository.

The lmod dependencies things listed there refer to those under 

/hpc/local/CentOS7/gen/software/etc/modulefiles 

and/or 

/hpc/local/CentOS7/pmc_research/software/etc/modulefiles 

The other dependencies are

* platediagnostics (R package)
* SCutils  (R package)

All have been git-cloned under /hpc/local/CentOS7/pmc_research/software
from https://bitbucket.org/princessmaximacenter/NAME.git 
(where NAME is demultiplex, platediagnostics, SCutils)

The R packages must be R CMD INSTALL-ed into the central $R_LIBS:

```
cd /hpc/local/CentOS7/pmc_research/software

rlibs=/hpc/local/CentOS7/gen/R_libs/3.6.1

pkgs="SCutils platediagnostics"

for pkg in $pkgs; do 
  pushd $pkg
  git pull
  popd
done

R CMD INSTALL -l $rlibs $pkags

```

(Note that platediagnostics itself is installed in under /hpc/local/CentOS7/pmc_research 
but the Rlibs are still under /hpc/local/CentOS7/gen since there is no good R under
/hpc/local/CentOS7/pmc_research/ yet)

