<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Sharq](#sharq)
- [Installation](#installation)
- [Running the pipeline](#running-the-pipeline)
- [Resulting files](#resulting-files)
- [Contents of this repository](#contents-of-this-repository)
  - [scripts/](#scripts)
  - [SCutils/](#scutils)
  - [data/](#data)
  - [platediagnostics](#platediagnostics)
  - [miscUtilities/](#miscutilities)
  - [gtfClustering/](#gtfclustering)
  - [docs/](#docs)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Sharq

This repository contains scripts and tools constituting a single-cell RNAseq pipeline written by Tito Candelli, Philip Lijnzaad and Thanasis Margaritis. It is described in more detail in
[Candelli et al., bioRxiv 2018](https://doi.org/10.1101/250811).
 
The pipeline was initially developed by Dominic Gr�n and Alexander van Oudenaarden at the
[Hubrecht Institute](www.hubrecht.eu/), Utrecht, The Netherlands.

**Most of the directories mentioned here have their own README.md file with details; see there.**


# Installation

The installation is currently very much *ad hack*.  The following components are required:

 * a working STAR installation (see https://github.com/alexdobin/STAR )
 * the subread package (for featureCount) (see http://subread.sourceforge.net/)
 * For execution using Cromwell + WDL:  a Cromwell installation (see https://github.com/broadinstitute/cromwell)
 * Alternatively, a standalone pipeline execution engine is provided (but not supported anymore), see the `pipeline.py` script 
 * Both Cromwell/WDL and `pipeline.py` require a Sun Grid Engine installation or similar (see https://arc.liv.ac.uk/trac/SGE )
 * The perl library `Set::Scalar` muts be available (available from CPAN)
 * the perl library `mismatch` must be available; this is available as part of the `demultiplex` repository, see https://bitbucket.org/princessmaximacenter/demultiplex
     * This in turn requires the perl libraries `Math::Combinatorics` and `Regexp::Optimizer`, both again available from CPAN
 * The R libraries `platediagnostics` and `SCutils`, both available from the current repository, must be available. Their dependencies are listed in their `DESCRIPTION` files. One of them is the `uuutils` package, get it from https://bitbucket.org/princessmaximacenter/uuutils .

In addition to the path elements needed for `STAR`, `featureCount`,
and `Cromwell`, your `$PATH` must contain the `scripts`
directory. `$PERLLIB` must also contain this `scripts` directory as well as the `demultiplex` directory.
A very convenient way to manage these things is `Lmod` (see https://lmod.readthedocs.io/ )
An example lmod script to set the appropriate environment for running Sharq is provided in the `miscUtilities` directory. 

The pipeline is currently configured using `json` files provided in the `data` directory. 
It it split in a general configuration part (`data/commonInputs.json`), and genome-specific 
part (currently `humanInputs.json` and `mouseInputs`, also in folder `data/`). Configuration of both should be trivial. 

Starting the pipeline requires specifying the species (see below), this translates to merging
`commonInputs.json` and `THESPECIESinputs.json`.

# Running the pipeline

The fastq files of your samples are expected to look like
`smp273-L1-R1.fastq.gz`. `smp273` is the sample name here, but that can of course
be anything. L1 refers to lane 1 (this can go up to
8).  R1 and R2 refer to read1 and read2; both must be present and be
sorted by name. If this concerns mouse data, the pipeline would be started
as follows:

```
$  nohup Sharq.py --species mouse smp273  > cromwell.out 2>cromwell.log &

```

The driver script will find *all* smp273-L[1-8]-R[12].fastq.gz files and
analyze them. The `nohup` and the `&` makes sure that the process
carries on even when you close the terminal session; standard output
goes to file `cromwell.out`, standard error to `cromwell.log`. 

Keep an eye on `cromwell.out` and `cromwell.log` (and/or use SGE's
`qstat` command) to track the progress of the pipeline.  (A good way to
check the process is to use `tail -f filename`, or `multitail file1 file2`.  

# Resulting files

The final results go to whatever is configured as
`Sharq.cleanup.outFolder` in `data/commonInputs.json`. For sample `smp273` its contents would be:

 * smp273-compTrim-tooshort.fastq.gz: the original read2 reads that had to be trimmed so much that they are no use anymore and were discarded
 * smp273.STARLog.final.out: the most important output from the STAR mapper
 * `smp273.*.log`: log output from the various stages (also used as input to platediagnostics). (In older runs these were called `*.elog`).
 * `smp273.bam`: the mapped reads, annotated with what feature (e.g. gene) they correspond to. Unmapped reads are kept in the `*.bam` file. The file is not sorted.
 * `smp273.saturation.txt, smp273.wellsat.genes.txt, smp273.wellsat.umis.txt`: used for the accumulation/saturation curves in the platediagnostics report
 * `smp273.platediagnostics.pdf`: the QC report. See  https://bitbucket.org/princessmaximacenter/sharq/src/master/platediagnostics/doc/README.md for full documentation )
 * `smp273.platediagnostics.warnings.txt`: warnings and error messages from the QC report

The count tables are:

* `smp273.rawcounts.txt`: The raw read counts (don't use them). Note: the first few lines (starting with '#') contain aggregate numbers, they are used by the QC script.
 * `smp273.umicounts.txt`: the raw UMI counts (don't use them).
 * `smp273.transcripts.txt`: Use these. They are the binomially corrected UMI counts, and are the best estimate of transcript abundance in your data.
 * `smp273.as_rawcounts.txt, smp273.as_umicounts.txt, smp273.as_transcripts.txt`: antisense mapping reads, currently not used.
 * `smp273.platediagnostics.stats.txt`: The 'Post-mapping statistics' table used in the QC, in tab-delimited format.
 * `smp273.platediagnostics.live.txt`: the subset of wells from smp273.transcripts.txt deemed contain material from live cells. This is based on the automatically determined 'liveness'-cutoff (see the QC report) and is not appropriate for in-depth analyses. 

The first column in the count tables is `UNK`, which represents all reads for which the well of origin could not be established.

# Contents of this repository

## scripts/
 * The scripts constituting most of the pipeline

## SCutils/
 * R library used by both platediagnostics and downstream analysis

## data/
 * Reference and configuration data (but not genomes!)

## platediagnostics
 * R library (with mostly plotting functions) for doing QC on raw-ish
   data such as produced by the pipeline scripts. The main driver script is called `platediagnostics.R`
   also found in this directory

## miscUtilities/
 * miscellaneous scripts. A number of them have have been moved to the non-public `sc-facility` repository as they are only of interest to the Princess Maxima single-cell sequencing facility

## gtfClustering/
 * cluster-GTFprotein_coding.R - Identify set of overlapping features with different IDs and merge them to prevent ambiguous assignment of reads to features (which would lead to discarding them)


## docs/
 * documents with some hints on how to produce the files currently in the `data/` directory
